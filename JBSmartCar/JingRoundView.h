//
//  JingRoundViewDelegate.h
//  Created by 肖建明 on 15/4/14.
//  Copyright (c) 2015年 DaKnag. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol JingRoundViewDelegate <NSObject>

-(void) playStatuUpdate:(BOOL)playState;

@end


@interface JingRoundView : UIView

@property (assign, nonatomic) id<JingRoundViewDelegate> delegate;

@property (strong, nonatomic) UIImage *roundImage;
@property (assign, nonatomic) BOOL isPlay;
@property (assign, nonatomic) float rotationDuration;
@property (strong, nonatomic) UIImageView *roundImageView;

@end
