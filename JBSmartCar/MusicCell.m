//
//  MusicCell.m
//  BL—05
//
//  Created by Apple on 15/11/26.
//  Copyright © 2015年 Apple. All rights reserved.
//

#import "MusicCell.h"

@interface MusicCell ()

@end

@implementation MusicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    appDele=(AppDelegate*)[UIApplication sharedApplication].delegate;   //根对象
    self.playEQView.tintColor = [UIColor orangeColor];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
   [super setSelected:selected animated:animated];
    if(selected){
       
        self.numLab.hidden = YES;
        self.playEQView.hidden = NO;
        self.musicName.textColor=[UIColor orangeColor];
        self.singName.textColor=[UIColor orangeColor];
        self.numLab.textColor=[UIColor orangeColor];
        self.timeLab.textColor=[UIColor orangeColor];
        
        if (appDele.filePlayer.channelIsPlaying) {
            
        self.playEQView.state = NAKPlaybackIndicatorViewStatePlaying;
            
        }else{
        
        self.playEQView.state = NAKPlaybackIndicatorViewStatePaused;
            
        }
        
        self.musicName.labelize = NO;
    }else{
//        [self setState:NAKPlaybackIndicatorViewStateStopped];
        self.numLab.hidden = NO;
        self.playEQView.hidden =YES;
        UIColor *color=[UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
        self.musicName.textColor=[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0];
        self.singName.textColor=color;
        self.numLab.textColor=color;
        self.timeLab.textColor=color;
        
        self.musicName.labelize = YES;
    }
}
- (void)setState:(NAKPlaybackIndicatorViewState)state {
    self.playEQView.state = state;
}

@end
