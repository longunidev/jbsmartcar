//
//  CustomControlView.m
//  CustomWheelView
//
//  Created by 肖建明 on 16/4/5.
//  Copyright © 2016年 Veryed. All rights reserved.
//

#import "CustomControlView.h"

@implementation CustomControlView

@synthesize startTransform;

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initImage];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self initImage];
    }
    return self;
}
-(void)initImage{
    self.pointImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fm_compass_pointer"]];
    self.pointImageView.contentMode = UIViewContentModeScaleAspectFill;//设置拉伸填充
    self.pointImageView.clipsToBounds = YES;
    
    self.backImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fm_compass"]];
    self.backImageView.contentMode = UIViewContentModeScaleAspectFill;//设置拉伸填充
    self.backImageView.clipsToBounds = YES;
}

-(void)layoutSubviews{
    if(self.subviews.count == 0){
        CGFloat vieWidth = MIN(self.frame.size.width, self.frame.size.height);
        mCenter = CGPointMake(vieWidth*0.5,vieWidth*0.5);
        self.backImageView.frame = CGRectMake(0, 0, vieWidth, vieWidth);
        self.backImageView.center = mCenter;
        [self addSubview:self.backImageView];
        
        self.container = [[UIView alloc] initWithFrame:self.frame];
        self.container.userInteractionEnabled = YES;
        self.container.center = mCenter;
        self.pointImageView.frame = CGRectMake(0, 0, vieWidth, vieWidth);
        self.pointImageView.center = self.container.center;
        [self.container addSubview:self.pointImageView];
        [self addSubview:self.container];
        startTransform = self.pointImageView.transform;
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self];
    float dx = touchPoint.x - mCenter.x;
    float dy = touchPoint.y - mCenter.y;
    deltaAngle = atan2(dy,dx);   //计算角度 反转的角度
    startTransform = self.pointImageView.transform;
    if(self.volueDelegate){
        if([self.volueDelegate respondsToSelector:@selector(wheelViewActionTouch:)]){
            [self.volueDelegate wheelViewActionTouch:self];   //通知协议开始进行触摸了
        }
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint pt = [touch locationInView:[touch view]];//获取触摸点
    float dx = pt.x  - mCenter.x;
    float dy = pt.y  - mCenter.y;
    float ang = atan2(dy,dx);
    float angleDifference = deltaAngle - ang;
    self.pointImageView.transform = CGAffineTransformRotate(startTransform,
                                                            -angleDifference);
    CGFloat radians = atan2f(self.pointImageView.transform.b,
                             self.pointImageView.transform.a);
    if(radians < 0){
        radians = M_PI*2 + radians;
    }
    int volue = (radians/(2*M_PI))*240+860;
    if(self.volueDelegate){
        if([self.volueDelegate respondsToSelector:@selector(wheelViewVolueChange: from:)]){
            [self.volueDelegate wheelViewVolueChange:volue from:self];
        }
    }
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    //获取触摸点
    CGPoint pt = [touch locationInView:[touch view]];
    float dx = pt.x  - mCenter.x;
    float dy = pt.y  - mCenter.y;
    float ang = atan2(dy,dx);
    float angleDifference = deltaAngle - ang;
    self.pointImageView.transform = CGAffineTransformRotate(startTransform, -angleDifference);
    CGFloat radians = atan2f(self.pointImageView.transform.b,
                             self.pointImageView.transform.a);
    if(radians < 0){
        radians = M_PI*2 + radians;
    }
    int volue = (radians/(2*M_PI))*240+860;
    if(self.volueDelegate){
        if([self.volueDelegate respondsToSelector:@selector(wheelViewVolueChange: from:)]){
            [self.volueDelegate wheelViewVolueChange:volue from:self];
        }
        if([self.volueDelegate respondsToSelector:@selector(wheelViewEndTouch:)]){
            [self.volueDelegate wheelViewEndTouch:self];
        }
    }
}
-(void)setFMValue:(int)fmValue{
    //外部设置值
    CGFloat mAngle = (fmValue - 860) /240.0f * ( 2 * M_PI);
    //NSLog(@"计算的角度:%f",mAngle);
    [UIView beginAnimations:nil context:nil];
    self.pointImageView.transform = CGAffineTransformMakeRotation(mAngle);
    [UIView commitAnimations];
}

@end
