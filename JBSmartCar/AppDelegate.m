//
//  AppDelegate.m
//  JBSmartCar
//
//  Created by xjm on 17/3/1.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import "AppDelegate.h"
#import "MusicBean.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "ViewController.h"
#import "ConnectionViewController.h"
#import "RateViewController.h"
#import "MusicViewController.h"
#import "TipHintView.h"
//#import "GoogleViewController.h"
//#import "CompassViewController.h"

//检测当前系统版本的宏
#define IS_IOS10_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize mBluzConnector;//蓝牙搜索
@synthesize mMediaManager;//应用模式接口

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];

    // 准备媒体音乐
    [self prepareAudioSession];
    //初始化音乐播放器
    self.audioController = [[AEAudioController alloc]
                            initWithAudioDescription:AEAudioStreamBasicDescriptionNonInterleaved16BitStereo];
    [self eqFilters];  //初始化
    [_audioController start:NULL];
    //接受系统的播放器信息显示   控制系统Toolbar的音乐控制
    self.audioController.allowMixingWithOtherApps = NO;
    
    [self initEQSettingValue];  //恢复之前的EQ值
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//    [self userNotification];
    //初始化蓝牙列表
    self.bluetoothDic = [[NSMutableArray alloc] init];
    //初始化音乐列表
    self.m_arrMusic=[[NSMutableArray alloc] init];
    self.m_nSelIdx = -1;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    ViewController *viewController = [[ViewController alloc] init];//分栏控制器
    //初始化不可以点击
//    viewController.tabBar.userInteractionEnabled = NO;
    viewController.tabBar.tintColor=[UIColor orangeColor];
    viewController.tabBar.barTintColor=[UIColor blackColor];
    
    //初始化默认的专辑图片
    self.defautImage = [UIImage imageNamed:@"ic_album_cover_default"];
    
    ConnectionViewController *connVC = [[ConnectionViewController alloc] init];
    RateViewController *rateVC = [[RateViewController alloc] init];
    MusicViewController *musicVC = [[MusicViewController alloc] init];
//    GoogleViewController *mapVC=[[GoogleViewController alloc] init];
//    CompassViewController *comVC=[[CompassViewController alloc] init];
    
    connVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Connect", nil) image:[UIImage imageNamed:@"btn_tap_link_normal"] selectedImage:[UIImage imageNamed:@"btn_tap_link_normal_orange"]];
    rateVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"FM", nil) image:[UIImage imageNamed:@"btn_tap_fm_normal"] selectedImage:[UIImage imageNamed:@"btn_tap_fm_normal_orange"]];
    musicVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Music", nil) image:[UIImage imageNamed:@"btn_tap_music_normal"] selectedImage:[UIImage imageNamed:@"btn_tap_music_normal_orange"]];
//    mapVC.tabBarItem=[[UITabBarItem alloc] initWithTitle:NSLocalizedString(@"Map", nil) image:[UIImage imageNamed:@"map_white"] selectedImage:nil];
//    comVC.tabBarItem=[[UITabBarItem alloc] initWithTitle:@"方向" image:[UIImage imageNamed:@"p_ring_white"] selectedImage:nil];
    [viewController setViewControllers:[NSArray arrayWithObjects:connVC,rateVC,musicVC, nil]];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    nav.navigationBar.hidden = YES; //隐藏导航条
    self.window.rootViewController = nav;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    //IOS10以上开始需要进行检查当前读取音乐列表的权限
    if (IS_IOS10_OR_LATER) {
        
     [self checkMediaLibraryPermissions];
        
    }else{
        
     [NSThread detachNewThreadSelector:@selector(getMusicMessage) toTarget:self withObject:nil];
    }
    return YES;
}

-(void)initEQSettingValue{
    //初始化当前的EQ值
    NSUserDefaults * user = [NSUserDefaults standardUserDefaults];
    float slider16K = [user floatForKey:@"slider16K"];
    if(slider16K != 0){
        [self setupEqFilter:self.eq16KHzFilter centerFrequency:16000 gain:slider16K];
    }
    float slider8K = [user floatForKey:@"slider8K"];
    if(slider8K != 0){
        [self setupEqFilter:self.eq8KHzFilter centerFrequency:8000 gain:slider8K];
    }
    float slider4K = [user floatForKey:@"slider4K"];
    if(slider4K != 0){
        [self setupEqFilter:self.eq4KHzFilter centerFrequency:4000 gain:slider4K];
    }
    float slider2K = [user floatForKey:@"slider2K"];
    if(slider2K != 0){
        [self setupEqFilter:self.eq2KHzFilter centerFrequency:2000 gain:slider2K];
    }
    float slider1K = [user floatForKey:@"slider1K"];
    if(slider16K != 0){
        [self setupEqFilter:self.eq1KHzFilter centerFrequency:1000 gain:slider1K];
    }
    float slider500 = [user floatForKey:@"slider500"];
    if(slider500 != 0){
        [self setupEqFilter:self.eq500HzFilter centerFrequency:500 gain:slider500];
    }
    float slider250 = [user floatForKey:@"slider250"];
    if(slider250 != 0){
        [self setupEqFilter:self.eq250HzFilter centerFrequency:250 gain:slider250];
    }
    float slider125 = [user floatForKey:@"slider125"];
    if(slider125 != 0){
        [self setupEqFilter:self.eq125HzFilter centerFrequency:125 gain:slider125];
    }
    float slider64 = [user floatForKey:@"slider64"];
    if(slider64 != 0){
        [self setupEqFilter:self.eq64HzFilter centerFrequency:64 gain:slider64];
    }
    float slider32 = [user floatForKey:@"slider32"];
    if(slider32 != 0){
        [self setupEqFilter:self.eq32HzFilter centerFrequency:32 gain:slider32];
    }
}

//准备音乐媒体
-(void)prepareAudioSession{
    
    [[AVAudioSession sharedInstance] setActive:NO error: nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback  error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error: nil];
}
//IOS检查当前读取音乐权限
-(void) checkMediaLibraryPermissions {
    [MPMediaLibrary requestAuthorization:^(MPMediaLibraryAuthorizationStatus status){
        if(status == MPMediaLibraryAuthorizationStatusAuthorized){//已经有权限了
            [NSThread detachNewThreadSelector:@selector(getMusicMessage) toTarget:self withObject:nil];
        }else if(status == MPMediaLibraryAuthorizationStatusDenied){//权限被拒绝了
            self.isMusicPermissionsDenied = YES;
        }
    }];
}
-(void)getMusicMessage{   //查询所有音乐信息
    MPMediaQuery *myPlaylistsQuery = [MPMediaQuery songsQuery];
    NSArray *playlists = [myPlaylistsQuery collections];
    for (MPMediaPlaylist *playlist in playlists) {
        NSArray *array = [playlist items];
        for (MPMediaItem *song in array) {
            MusicBean *bean = [[MusicBean alloc] init];
            MPMediaItemArtwork *artwork = [song valueForProperty: MPMediaItemPropertyArtwork];
            UIImage *artworkImages = [artwork imageWithSize:CGSizeMake(320, 265)];
            if (artworkImages) {
                bean.m_musicCover = (UIImage *)artworkImages;
            }
            bean.m_musicName = [song valueForProperty: MPMediaItemPropertyTitle];
            bean.m_url =  [song valueForProperty: MPMediaItemPropertyAssetURL];
            bean.m_singerName = [song valueForKey:MPMediaItemPropertyArtist];
            if(bean.m_singerName.length==0){
                bean.m_singerName=@"<unknown>";
            }
            //计算音乐文件所需要的时间
            CGFloat dblTotal=[[song valueForKey:MPMediaItemPropertyPlaybackDuration] floatValue];
            int seconds = (int)dblTotal;
            int minute = 0;
            if (seconds >= 60) {
                int index1 = seconds / 60;
                minute = index1;
                seconds = seconds - index1 * 60;
            }
            bean.m_musicTime = [NSString stringWithFormat:@"%02d:%02d", minute, seconds];
            [self.m_arrMusic addObject:bean];
        }
    }

    
    [NSThread exit];//关闭线程
}

-(NSArray<AEParametricEqFilter *> *)eqFilters{
    if(!_eqFilters){
        _eq16KHzFilter  = [[AEParametricEqFilter alloc] init];
        _eq8KHzFilter  = [[AEParametricEqFilter alloc] init];
        _eq4KHzFilter = [[AEParametricEqFilter alloc] init];
        _eq2KHzFilter = [[AEParametricEqFilter alloc] init];
        _eq1KHzFilter = [[AEParametricEqFilter alloc] init];
        _eq500HzFilter  = [[AEParametricEqFilter alloc] init];
        _eq250HzFilter  = [[AEParametricEqFilter alloc] init];
        _eq125HzFilter  = [[AEParametricEqFilter alloc] init];
        _eq64HzFilter = [[AEParametricEqFilter alloc] init];
        _eq32HzFilter = [[AEParametricEqFilter alloc] init];
        _eqFilters     = @[_eq16KHzFilter, _eq8KHzFilter, _eq4KHzFilter, _eq2KHzFilter, _eq1KHzFilter, _eq500HzFilter, _eq250HzFilter, _eq125HzFilter, _eq64HzFilter, _eq32HzFilter];
    }
    return _eqFilters;
}
- (void)setupEqFilter:(AEParametricEqFilter *)eqFilter centerFrequency:(double)centerFrequency gain:(double)gain {
    if (![_audioController.filters containsObject:eqFilter] ) {
        for (AEParametricEqFilter *existEqFilter in _eqFilters) {
            if (eqFilter == existEqFilter) {
                [_audioController addFilter:eqFilter];
                break;
            }
        }
    }
    eqFilter.centerFrequency = centerFrequency;
    eqFilter.qFactor         = 1.0;
    eqFilter.gain            = gain;
}
//播放歌曲
-(void)playSong:(NSInteger) playMode{
    if([self.m_arrMusic count] == 0){   //音乐长度为0，不能播放
        return;
    }
    if(playMode == MusicPlayStateModePlay){
        if(_filePlayer && !_filePlayer.channelIsPlaying){
            _filePlayer.channelIsPlaying = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"playMusicChange" object:nil];
        
        }
        return;
    }else if(playMode == MusicPlayStateModePause){   //暂停
        if(_filePlayer && _filePlayer.channelIsPlaying){
            _filePlayer.channelIsPlaying = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"playMusicChange" object:nil];
        }
        return;
    }else{
        if(playMode == MusicPlayStateModePrevious){    //上一首
            if(self.playRepeatMode == MusicPlayRepeatShuffle){
                self.m_nSelIdx =arc4random_uniform((unsigned)[self.m_arrMusic count]);   //生成0-(x-1)之间的
            }else{
                self.m_nSelIdx -= 1;
            }
        }else if(playMode == MusicPlayStateModeNext){   //下一首
            if(self.playRepeatMode == MusicPlayRepeatShuffle){
                self.m_nSelIdx =arc4random_uniform((unsigned)[self.m_arrMusic count]);   //生成0-(x-1)之间的
            }else{
                self.m_nSelIdx += 1;
            }
        }
    }
    if(self.m_nSelIdx < 0 || self.m_nSelIdx >= self.m_arrMusic.count){
        self.m_nSelIdx = 0;
    }
    self.playBean = [self.m_arrMusic objectAtIndex:_m_nSelIdx];
    
    //NSLog(@"播放文件地址:%@",self.playBean.m_url);
    
    if (_filePlayer) {
        _filePlayer.channelIsPlaying = NO;
        [_audioController removeChannels:@[_filePlayer]];
        _filePlayer = nil;
    }

    // 创建AEAudioFilePlayer对象
    NSError *error = NULL;
    _filePlayer = [[AEAudioFilePlayer alloc] initWithURL:self.playBean.m_url error:&error];
    _filePlayer.removeUponFinish = YES;
    _filePlayer.volume = 1.0f;
    if(!error){
        __weak typeof (self)weakself = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakself.audioController addChannels:@[weakself.filePlayer]];
            // 进行播放
            [weakself.filePlayer setCompletionBlock:^{
                if(self.playRepeatMode == MusicPlayRepeatSingle){
                    
                    [weakself playSong:MusicPlayStateModeNew];   //播放完成
                    
                }else{
                    
                    [weakself playSong:MusicPlayStateModeNext];   //播放完成
                }
            }];
        });
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playMusicChange" object:nil];
    }else{
        if(self.m_arrMusic > 0 && self.m_nSelIdx >= (self.m_arrMusic.count-1)){
            [self playSong:MusicPlayStateModeNext];
        }
    }
}

//远程事件控制
-(void)remoteControlReceivedWithEvent:(UIEvent *)event{
    if (event.type == UIEventTypeRemoteControl) {
        if (event.subtype == UIEventSubtypeRemoteControlPlay || event.subtype == UIEventSubtypeRemoteControlPause) {
            //播放暂停
            if (self.filePlayer.channelIsPlaying) {
                
                [self playSong:MusicPlayStateModePause];//暂停
        
                
            }else{
                [self playSong:MusicPlayStateModePlay];//播放
            }
        }else if (event.subtype == UIEventSubtypeRemoteControlNextTrack){
            //下一首
            
            [self playSong:MusicPlayStateModeNext];
            
            
        }else if(event.subtype == UIEventSubtypeRemoteControlPreviousTrack){
            //上一首
            
            [self playSong:MusicPlayStateModePrevious];
        }
    }
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    if (mBluzConnector !=nil) {
        [mBluzConnector setAppForeground:NO];
    }
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    if (mBluzConnector != nil) {
        [mBluzConnector setAppForeground:NO];
    }
    
    [TipHintView hideTipHint];

}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    if (mBluzConnector != nil) {
        [mBluzConnector setAppForeground:YES];  // 设置前后台
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    if (mBluzConnector != nil) {
        
        [mBluzConnector setAppForeground:YES];
    }
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    if(self.globalManager){
        
        if (currentA2dp!=0) {
            
          isfirstA2dp=YES;
          int queKey = [self.globalManager buildKey:QUE cmdID:131];
          [self.globalManager sendCustomCommand:queKey param1:0 param2:0 others:nil];
            
        }
    }
}
- (void)applicationWillTerminate:(UIApplication *)application {
    
}




@end
