//
//  TopAlert.m
//  TestAlertTopView
//
//  Created by 肖建明 on 2016/12/6.
//  Copyright © 2016年 Veryed. All rights reserved.
//

#import "TopAlert.h"

@interface TopAlert()
@property(nonatomic,strong)UILabel *contentTextLabel;
@end
@implementation TopAlert

- (instancetype)initWithStyle:(NSString *)contentStr {
    self = [super init];
    if (self) {
        [self initWindow:contentStr];
    }
    return self;
}

-(void)initWindow:(NSString *)contentStr{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    [window addSubview:self];
    
    [self addSubview:self.contentTextLabel];
    [self setBackgroundColor:[UIColor orangeColor]];
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(65);
        make.width.mas_equalTo(window);
    }];
    [self.contentTextLabel setFont:[UIFont systemFontOfSize:14]];
    [self.contentTextLabel setTextColor:[UIColor whiteColor]];
    [self.contentTextLabel sizeToFit];
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:contentStr];
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentStr length])];
    [self.contentTextLabel setAttributedText:attributedString];
    
    [self.contentTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.bottom.equalTo(self).offset(-8);
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
    }];
    [self.contentTextLabel setAlpha:0];
};
-(void)showInfo{
    self.transform = CGAffineTransformMakeTranslation(0, -80);
    [UIView animateWithDuration:0.35 animations:^{
        self.transform = CGAffineTransformMakeTranslation(0, 0);
        [self.contentTextLabel setAlpha:1];
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self TopAlertAnimateSenior];
    });
}

-(void)TopAlertAnimateSenior{
    [UIView animateWithDuration:0.35 animations:^{
        self.transform = CGAffineTransformMakeTranslation(0, -80);
        [self.contentTextLabel setAlpha:0];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(UILabel*)contentTextLabel {
    if (_contentTextLabel == nil) {
        _contentTextLabel = [[UILabel alloc]init];
        [_contentTextLabel setFont:[UIFont systemFontOfSize:13]];
        [_contentTextLabel setTextColor:[UIColor whiteColor]];
        _contentTextLabel.numberOfLines = 0;
    }
    return _contentTextLabel;
}

@end
