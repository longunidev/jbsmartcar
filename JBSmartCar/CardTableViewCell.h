//
//  CardTableViewCell.h
//  LTGM
//
//  Created by 肖建明 on 15/12/29.
//  Copyright © 2015年 wangwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NAKPlaybackIndicatorView.h"
#import "MarqueeLabel.h"

@interface CardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *indexLable;
@property (weak, nonatomic) IBOutlet MarqueeLabel *musicName;
@property (weak, nonatomic) IBOutlet NAKPlaybackIndicatorView *tfPlayEQ;

@end
