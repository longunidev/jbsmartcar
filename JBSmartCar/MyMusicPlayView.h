//
//  MyMusicPlayView.h
//  LTGM
//
//  Created by wangwei on 15/12/1.
//  Copyright © 2015年 wangwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MusicBean.h"
#import "JingRoundView.h"
#import "CNPPopupController.h"

@interface MyMusicPlayView : UIViewController<CNPPopupControllerDelegate>{
    AppDelegate* appDele;   //根对象，获取蓝牙连接对象
    BOOL m_bPlay;           //是否正在播放
    NSTimer *timer;         //监控音频播放进度
}
@property(nonatomic,strong)MusicBean* m_musicBean;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *singername;
@property (weak, nonatomic) IBOutlet UILabel *dur;
@property (weak, nonatomic) IBOutlet UILabel *end;
@property (weak, nonatomic) IBOutlet UISlider *slider;

@property (weak, nonatomic) IBOutlet UIButton *play;
@property (weak, nonatomic) IBOutlet JingRoundView *roundView;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;

@property (nonatomic, strong) CNPPopupController *soundController;
@property (weak, nonatomic) IBOutlet UISlider *soundSliderView;
@property (weak, nonatomic) IBOutlet UIButton *repeatButton;

@end
