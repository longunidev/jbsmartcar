//
//  ViewController.h
//  JBSmartCar
//
//  Created by xjm on 17/3/1.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainDateValueChangleDelegate <NSObject>
-(void)mainDateValueChangle:(int)date1 :(int)date2;
@end

@interface ViewController : UITabBarController

@property (nonatomic,weak) id<MainDateValueChangleDelegate> valueDelegate;

@end

