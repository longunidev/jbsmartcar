//
//  AppDelegate.h
//  JBSmartCar
//
//  Created by xjm on 17/3/1.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BluzDevice.h"
#import "BluzManager.h"
#import "TheAmazingAudioEngine.h"
#import "AEParametricEqFilter.h"
#import "MusicBean.h"
#import <CoreLocation/CoreLocation.h>

typedef NS_ENUM(NSUInteger, MusicPlayType) {
    MusicPlayStateModePlay =  0,    //会自动为0 后面自动递增
    MusicPlayStateModePause,
    MusicPlayStateModeNext,
    MusicPlayStateModePrevious,
    MusicPlayStateModeNew          //强制进行播放新的
};
typedef NS_ENUM(NSUInteger, MusicRepeatType) {
  
    MusicPlayRepeatList=0,       //列表播放
    MusicPlayRepeatShuffle ,    //随机播放
    MusicPlayRepeatSingle     //单曲循环
};

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    @public BOOL hasCard;   //判断是否有卡
    @public BOOL hasUPan;   //判断是否有U盘
    @public int maxVolue;
    @public int minVoloe;
    @public int curr_volue;  //当前音量
    @public int currentV;    //当前电压
    @public int currentFM;   //当前FM频道
    @public int curr_mode;   //当前模式
    @public BOOL isMute;     //当前是否静音模式
    @public int currCardMode;//当前TF播放模式
    @public BOOL isfirstA2dp;    //防止多次调用
    @public int  currentPage;
    @public int  currentA2dp;
   
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) BluzDevice *mBluzConnector;//负责蓝牙搜索、连接等操作的对象
@property (strong, nonatomic) BluzManager *mMediaManager;//获取各应用模式管理接口
@property (strong, nonatomic) id<GlobalManager> globalManager;//全局控制接口
@property (strong, nonatomic) id<MusicManager> musicManager;

@property (strong,nonatomic) NSMutableArray *bluetoothDic;    //搜索到的蓝牙列表
@property (strong,nonatomic) NSString *autoConnManuter;       //自动连接的地址
@property (strong,nonatomic) CBPeripheral *connCBPeripheral;  //已经连接的蓝牙对象
@property (nonatomic,assign) BOOL isOpenBlue;   //蓝牙是否开启状态

@property (strong,nonatomic) NSMutableArray* m_arrMusic;      //音乐列表
@property (nonatomic,assign)  BOOL isMusicPermissionsDenied;   //当前权限是否被拒绝

@property (nonatomic, strong) AEAudioController *audioController;
@property (nonatomic, strong) AEAudioFilePlayer *filePlayer;

@property (nonatomic, strong) AEParametricEqFilter *eq16KHzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq8KHzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq4KHzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq2KHzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq1KHzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq500HzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq250HzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq125HzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq64HzFilter;
@property (nonatomic, strong) AEParametricEqFilter *eq32HzFilter;
@property (nonatomic, strong) NSArray<AEParametricEqFilter *> *eqFilters;
//设置EQ的方法
-(void)setupEqFilter:(AEParametricEqFilter *)eqFilter centerFrequency:(double)centerFrequency gain:(double)gain;
-(void)playSong:(NSInteger) playMode;                         //音乐播放方法
@property (assign, nonatomic) int playRepeatMode;             //播放本地音乐循环模式
@property (assign, nonatomic) int m_nSelIdx;                  //当前播放下标
@property (nonatomic, weak) MusicBean *playBean;              //当前正在播放的

//防止第二次进入，重新加载播放
@property (nonatomic,assign)BOOL firstDouban;

@property(nonatomic,assign)int m_nMusicState;                 //当前TF播放状态

@property(nonatomic,strong) UIImage *defautImage;    //默认图片



@end

