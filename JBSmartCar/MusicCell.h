//
//  MusicCell.h
//  BL—05
//
//  Created by Apple on 15/11/26.
//  Copyright © 2015年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NAKPlaybackIndicatorView.h"
#import "AppDelegate.h"
#import "MarqueeLabel.h"

@interface MusicCell : UITableViewCell{
    AppDelegate *appDele;
}
@property (weak, nonatomic) IBOutlet MarqueeLabel *musicName;
@property (weak, nonatomic) IBOutlet UILabel *singName;
@property (weak, nonatomic) IBOutlet UILabel *numLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet NAKPlaybackIndicatorView *playEQView;

@end
