//
//  MusicBean.h
//  BluetoothBox
//
//  Created by huangdw on 13-12-26.
//  Copyright (c) 2013年 Halong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface MusicBean : NSObject

@property(nonatomic,strong)NSString *m_musicName;   //歌曲名
@property(nonatomic,strong)NSURL *m_url;            //歌曲地址
@property(nonatomic,strong)NSString *m_singerName;  //演唱者
@property(nonatomic,strong)NSString *m_musicTime;   //总时间
@property(nonatomic,strong)UIImage *m_musicCover;   //封面
@property(nonatomic,assign)BOOL isXiMaLaYaDownLoader;  //是否是喜马拉雅SDK下载的
@property(nonatomic,assign)NSString *imageURL;      //喜马拉雅SDK下载的URL

@end
