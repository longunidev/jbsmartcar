//
//  PlayListViewController.h
//  JBSmart_Car
//
//  Created by yangxiao on 16/7/9.
//  Copyright © 2016年 yangxiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface PlayListViewController : UIViewController{
   AppDelegate *appDete;
}
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UITableView *playTableView;
@property (nonatomic,strong)NSNumber *playID;
@property (nonatomic,strong)NSString *titleStr;
@property (nonatomic,strong)NSMutableArray *listArr;
@end
