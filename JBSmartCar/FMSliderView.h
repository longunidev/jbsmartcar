//
//  FMSliderView.h
//  CCA
//
//  Created by 肖建明 on 16/7/12.
//  Copyright © 2016年 Veryed. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FMSliderView;
@protocol CustomViewChangleDelegate <NSObject>
-(void)fmVolueChangeDelegate:(FMSliderView *)fMSliderView :(int)volue;
-(void)sliderViewActionTouch:(FMSliderView *)customControlView;
-(void)sliderViewEndTouch:(FMSliderView *)customControlView;
@end

@interface FMSliderView : UIControl

@property(strong,nonatomic)UIImageView *sliderImageView;  //背景图片
@property(strong,nonatomic)UIImageView *pointImageView;  //进度条图片
@property (nonatomic,weak) id<CustomViewChangleDelegate> volueDelegate;

-(void)setFMValue:(int)value;

@end
