//
//  RateViewController.m
//  JBSmartCar
//
//  Created by xjm on 17/3/1.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import "RateViewController.h"
#import "AppDelegate.h"
#import "CNPPopupController.h"
#import "FMSliderView.h"
#import "FMValueDB.h"
#import "CustomControlView.h"
#import "UIView+Toast.h"
#import "UIView+Extension.h"
#import "ViewController.h"
#import "CardViewController.h"
#import "RateTableViewCell.h"

@interface RateViewController ()<CNPPopupControllerDelegate,UITableViewDataSource,UITableViewDelegate,CustomViewChangleDelegate,CustomWheelViewChangleDelegate,MainDateValueChangleDelegate>{
    AppDelegate *appDele;
    NSTimer *queTimer;   //查询线程
    NSMutableArray *fmArray;
    FMValueDB *fmDBHelper;
    BOOL isRetention;  //是否缓存没有发送的数据
    BOOL isCanSend;    //是否可以发送数据
}

@property (nonatomic, strong) CNPPopupController *fmController;
@property (weak, nonatomic) IBOutlet UITableView *FMtableView;
@property (weak, nonatomic) IBOutlet UILabel *voltageLab;//电压值
@property (weak, nonatomic) IBOutlet UILabel *rateFMLab;
@property (weak, nonatomic) IBOutlet UILabel *wheelFMLab;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segemtView;
@property (weak, nonatomic) IBOutlet UIView *sliderChangeView;
@property (weak, nonatomic) IBOutlet UIView *wheelChangeView;

@property (weak, nonatomic) IBOutlet FMSliderView *sliderView;
@property (weak, nonatomic) IBOutlet CustomControlView *wheelView;
@property(nonatomic,retain) dispatch_source_t timer;   //倒计时计时器

@property (weak, nonatomic) IBOutlet UIButton *uPanButton;
@property (weak, nonatomic) IBOutlet UIButton *tfCardButton;

@property (nonatomic,assign) BOOL isShowing;    //当前页面是否显示状态。决定是否进行查询
@property (weak, nonatomic) IBOutlet UILabel *wheelVLabel;
@property (weak, nonatomic) IBOutlet UILabel *sliderVLabel;

@property (weak, nonatomic) IBOutlet UIView *sliderViewParent;

@end

@implementation RateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDele = (AppDelegate *)[UIApplication sharedApplication].delegate;
    fmArray = [[NSMutableArray alloc] initWithCapacity:0];
    //设置代理
    [self.sliderView setVolueDelegate:self];
    [self.wheelView setVolueDelegate:self];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    __weak typeof (self)weakself = self;
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1 * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        [weakself setCanSendColorBool];
    });
    dispatch_resume(_timer);
    //设置底层的数据改变监听
    ViewController *parentView = (ViewController *)[self parentViewController];
    [parentView setValueDelegate:self];
    
    if(appDele->hasCard){
        self.tfCardButton.hidden = NO;
    }
    if(appDele->hasUPan){
        self.uPanButton.hidden = NO;
    }
    //接受TF U盘改变通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hotplugUSBChanged) name:@"hotplugUSBChanged" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hotplugUhostChanged) name:@"hotplugUhostChanged" object:nil];
    
    fmDBHelper = [[FMValueDB alloc] init];
    [fmDBHelper opneDatabase];
    [fmArray addObjectsFromArray:[fmDBHelper getAllFM]];
    
    //监听蓝牙被断开了
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectionBluetooth) name:@"disconnectionBluetooth" object:nil];
    
}

-(void)disconnectionBluetooth{
    if(!appDele.connCBPeripheral && !appDele.globalManager){
        self.tfCardButton.hidden = YES;
        self.uPanButton.hidden = YES;
        appDele->hasCard = NO;
        appDele->hasUPan = NO;
    }
}

-(void)dealloc{
    [fmDBHelper closeDB];
    fmDBHelper = nil;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.isShowing = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.isShowing = NO;
}

-(void)hotplugUSBChanged{
    if(appDele->hasCard){
        self.tfCardButton.hidden = NO;
    }else{
        self.tfCardButton.hidden = YES;
    }
}
-(void)hotplugUhostChanged{
    
    if(appDele->hasUPan){
        
        self.uPanButton.hidden = NO;
        
    }else{
        
        self.uPanButton.hidden = YES;
        
    }
}
//u盘按钮被点击
- (IBAction)uPanButtonClick:(id)sender {
    if(appDele.globalManager){
        if(appDele->curr_mode != MODE_UHOST){
            [self.navigationController.view makeToastActivity:CSToastPositionCenter];
            [appDele.globalManager setMode:MODE_UHOST];
        }else{
            [self.navigationController pushViewController:[[CardViewController alloc] init] animated:YES];
        }
    }
}
- (IBAction)tfButtonClick:(id)sender {
    if(appDele.globalManager){
        if(appDele->curr_mode != MODE_CARD){
            [self.navigationController.view makeToastActivity:CSToastPositionCenter];
            [appDele.globalManager setMode:MODE_CARD];
        }else{
            [self.navigationController pushViewController:[[CardViewController alloc] init] animated:YES];
        }
    }
}

-(void)mainDateValueChangle:(int)date1 :(int)date2{
    //NSLog(@"得到当前底层返回的值:%i    %i",date1,date2);
    if(appDele->currentFM != date1){
        
        [self.sliderView setFMValue:date1];
        [self.wheelView setFMValue:date1];
        NSString *valueStr = [NSString stringWithFormat:@"%.1f",date1/10.0f];
        _rateFMLab.text = valueStr;
        _wheelFMLab.text = valueStr;
        appDele->currentFM = date1;
        //NSLog(@"底层过去的电压值:%i",date2);
    }
    if(appDele->currentV != date2 && date2 <= 320 && date2 > 0){
        self.sliderVLabel.hidden = NO;
        self.sliderVLabel.y = self.sliderViewParent.y -self.sliderVLabel.height;
        self.wheelVLabel.hidden = NO;
        appDele->currentV = date2 & 0xffff;
        [self.sliderVLabel setText:[NSString stringWithFormat:@"%.1fV",appDele->currentV/10.0f]];
        [self.wheelVLabel setText:[NSString stringWithFormat:@"%.1fV",appDele->currentV/10.0f]];
        
    }
}

-(void)fmVolueChangeDelegate:(FMSliderView *)fMSliderView :(int)volue{
    if(appDele->currentFM == volue){
        return;   //减少数据量发送
    }
    appDele->currentFM = volue;
    NSString *valueStr = [NSString stringWithFormat:@"%.1f",appDele->currentFM/10.0f];
    _rateFMLab.text = valueStr;
    _wheelFMLab.text = valueStr;
    [self.wheelView setFMValue:volue];
    if(isCanSend){
        [self sendFMVoloe:appDele->currentFM];
    }else{
        isRetention = YES;
    }
}
-(void)sliderViewActionTouch:(FMSliderView *)customControlView{
    
}
-(void)sliderViewEndTouch:(FMSliderView *)customControlView{
    
}
-(void)wheelViewActionTouch:(CustomControlView *)customControlView{
    
}
-(void)wheelViewEndTouch:(CustomControlView *)customControlView{
    
}

//圆盘数值改变
-(void)wheelViewVolueChange:(int)volue from:(CustomControlView *)view{
    if(appDele->currentFM == volue){
        return;   //减少数据量发送
    }
    appDele->currentFM = volue;
    NSString *valueStr = [NSString stringWithFormat:@"%.1f",appDele->currentFM/10.0f];
    _rateFMLab.text = valueStr;
    _wheelFMLab.text = valueStr;
    [self.sliderView setFMValue:volue];
    if(isCanSend){
        [self sendFMVoloe:appDele->currentFM];
    }else{
        isRetention = YES;
    }
}

//发送FM
- (void)sendFMVoloe:(int) fmVolue{
    if(appDele.globalManager){
        int key= [appDele.globalManager buildKey:SET cmdID:129];
        [appDele.globalManager sendCustomCommand:key param1:fmVolue param2:fmVolue others:nil];
        isCanSend = NO;
        isRetention = NO;
    }
}

-(void)setCanSendColorBool{
    //开始查询数据
    if (isRetention) {
        [self sendFMVoloe:appDele->currentFM];
        isCanSend = NO;    //可以发送数据
        isRetention = NO;    //是否滞留数据了
    }else{
        isCanSend = YES;
        //没有缓存数据的时候就进行查询   且必须在本页面可视情况下
        if(appDele.globalManager && self.isShowing){
            //发送查询
            int queKey = [appDele.globalManager buildKey:QUE cmdID:131];
            [appDele.globalManager sendCustomCommand:queKey param1:0 param2:0 others:nil];
        }
    }
}

- (IBAction)preBtnCilick:(id)sender {
    if(appDele.globalManager){
        if(appDele->currentFM < 860 || appDele->currentFM > 1100){
            appDele->currentFM = 860;
        }
        if (appDele->currentFM-1 < 860) {
            appDele->currentFM = 1100;
        }else{
            appDele->currentFM = appDele->currentFM - 1;
        }
        NSString *valueStr = [NSString stringWithFormat:@"%.1f",appDele->currentFM/10.0f];
        _rateFMLab.text = valueStr;
        _wheelFMLab.text = valueStr;
        //构建key成功
        [self sendFMVoloe:appDele->currentFM];
        [self.sliderView setFMValue:appDele->currentFM];
        [self.wheelView setFMValue:appDele->currentFM];
    }else{
        [self showConnectionDialog];
    }
}
- (IBAction)nextBtnCilick:(id)sender {
    if(appDele.globalManager){
        if(appDele->currentFM < 860 || appDele->currentFM > 1100){
            appDele->currentFM = 860;
        }
        if (appDele->currentFM+1 > 1100) {
            appDele->currentFM = 860;
        }else{
            appDele->currentFM = appDele->currentFM+1;
        }
        NSString *valueStr = [NSString stringWithFormat:@"%.1f",appDele->currentFM/10.0f];
        _rateFMLab.text = valueStr;
        _wheelFMLab.text = valueStr;
        //构建key成功
        [self sendFMVoloe:appDele->currentFM];
        [self.sliderView setFMValue:appDele->currentFM];
        [self.wheelView setFMValue:appDele->currentFM];
    }else{
        [self showConnectionDialog];
    }
}
- (IBAction)addFMBtnCilick:(id)sender {
    if(appDele.globalManager){
        for (NSNumber *fmNumber in fmArray) {
            if([fmNumber intValue] == appDele->currentFM){
                [self.navigationController.view makeToast:NSLocalizedString(@"fm_exists", nil) duration:2.0 position:CSToastPositionCenter];
                return;  //已经存在了
            }
        }
        if([fmDBHelper addFMValue:appDele->currentFM]){
            [self.navigationController.view makeToast:NSLocalizedString(@"add_success", nil) duration:2.0 position:CSToastPositionCenter];
            //加入到列表
            [fmArray addObject:[NSNumber numberWithInt:appDele->currentFM]];
        }else{
            [self.navigationController.view makeToast:NSLocalizedString(@"add_fail", nil) duration:2.0 position:CSToastPositionCenter];
        }
    }else{
        [self showConnectionDialog];
    }
}
//让底层停止发射
- (IBAction)breakBtnCilick:(id)sender {
    if(appDele.globalManager){
        [appDele.globalManager switchMute];   //设置静音状态
    }else{
        [self showConnectionDialog];
    }
}
- (IBAction)listBtnCilick:(id)sender {
    if(appDele.globalManager){
        if(!self.fmController){
            UIView *fmListView = [[[NSBundle mainBundle] loadNibNamed:@"FMListView" owner:self options:nil] objectAtIndex:0];
            fmListView.frame = CGRectMake(0, 0, self.view.frame.size.width, fmListView.height);
            self.fmController = [[CNPPopupController alloc] initWithContents:@[fmListView]];
            self.fmController.theme = [CNPPopupTheme defaultTheme];
            self.fmController.theme.popupStyle = CNPPopupStyleActionSheet;
            self.fmController.delegate = self;
            
            self.FMtableView.rowHeight = 50.0f;
            [self.FMtableView registerNib:[UINib nibWithNibName:@"RateTableViewCell" bundle:nil] forCellReuseIdentifier:@"FMTableCell"];
            self.FMtableView.delegate = self;
            self.FMtableView.dataSource = self;
            self.FMtableView.separatorColor=[UIColor colorWithWhite:0.8 alpha:0.2];
        }
        [self.FMtableView reloadData];
        [self.fmController presentPopupControllerAnimated:YES];
    }else{
        [self showConnectionDialog];
    }
}

- (IBAction)segmentVolueChange:(UISegmentedControl *)sender {
    if(sender.selectedSegmentIndex == 0){
        [self.sliderChangeView setHidden:NO];
        [self.wheelChangeView setHidden:YES];
    }else{
        [self.sliderChangeView setHidden:YES];
        [self.wheelChangeView setHidden:NO];
    }
}

//列表对话框   最近播放列表展示
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return fmArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FMTableCell"];
    NSNumber *fmValue = [fmArray objectAtIndex:indexPath.row];
    NSString *cellStr = [NSString stringWithFormat:@"FM %.1f %@",[fmValue intValue]/10.0f,@"MHZ"];
    cell.fmLabel.text =cellStr;
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.deleteButton addTarget:self action:@selector(deleteButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.deleteButton.tag = indexPath.row;
    
    if(appDele->currentFM == [fmValue intValue]){
        cell.isSelectFM = YES;
    }else{
        cell.isSelectFM = NO;
    }
    return cell;
}

-(void)deleteButtonClick:(UIButton *)button{
    NSNumber *fmValue = [fmArray objectAtIndex:button.tag];
    if([fmDBHelper deleteFmValue:[fmValue intValue]]){
        [fmArray removeObjectAtIndex:button.tag];
        [self.FMtableView reloadData];
        [self.navigationController.view makeToast:NSLocalizedString(@"delete_success", nil) duration:2.0 position:CSToastPositionCenter];
    }else{
        [self.navigationController.view makeToast:NSLocalizedString(@"delete_fail", nil) duration:2.0 position:CSToastPositionCenter];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(appDele.globalManager){
        NSNumber *fmValue = [fmArray objectAtIndex:indexPath.row];
        if(appDele->currentFM != [fmValue intValue]){
            appDele->currentFM = [fmValue intValue];
            [self sendFMVoloe:appDele->currentFM]; //发送出去
            //设置本地UI的显示
            NSString *valueStr = [NSString stringWithFormat:@"%.1f",appDele->currentFM/10.0f];
            _rateFMLab.text = valueStr;
            _wheelFMLab.text = valueStr;
            [self.sliderView setFMValue:appDele->currentFM];
            [self.wheelView setFMValue:appDele->currentFM];
            [tableView reloadData];
            
            [tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];//设置选中列表
        }
    }else{
        [self showConnectionDialog];
    }
}

-(void)showConnectionDialog{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"error_content_text", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"ok_text", nil) otherButtonTitles:nil, nil];
    [alertView show];   //显示对话框
}

@end
