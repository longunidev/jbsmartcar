//
//  CustomControlView.h
//  CustomWheelView
//
//  Created by 肖建明 on 16/4/5.
//  Copyright © 2016年 Veryed. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomControlView;

/**
 自定义View用户协议
 */
@protocol CustomWheelViewChangleDelegate <NSObject>
-(void)wheelViewActionTouch:(CustomControlView *)customControlView;
-(void)wheelViewEndTouch:(CustomControlView *)customControlView;
-(void)wheelViewVolueChange:(int)volue from:(CustomControlView *) view;
@end

@interface CustomControlView : UIControl{
    CGPoint mCenter;
    float deltaAngle;
}

@property (nonatomic,weak) id<CustomWheelViewChangleDelegate> volueDelegate;

@property (nonatomic,strong) UIImageView *pointImageView;   //指针图片

@property (nonatomic,strong) UIImageView *backImageView;   //罗盘图片

@property CGAffineTransform startTransform;

@property (nonatomic, strong) UIView *container;   //房子UIImageView旋转的时候放大

-(void)setFMValue:(int)fmValue;    //外部设置FM值

@end
