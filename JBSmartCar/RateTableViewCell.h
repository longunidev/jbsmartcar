//
//  RateTableViewCell.h
//  JBSmartCar
//
//  Created by xjm on 17/3/16.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RateTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *fmLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (assign,nonatomic) BOOL isSelectFM;

@end
