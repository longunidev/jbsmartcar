//
//  MyMusicPlayView.m
//  LTGM
//
//  Created by wangwei on 15/12/1.
//  Copyright © 2015年 wangwei. All rights reserved.
//

#import "MyMusicPlayView.h"
#import "UIView+Extension.h"
#import "UIView+Toast.h"
#import "UIView+Animations.h"


@interface MyMusicPlayView ()<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *soundPopLable;
@end

@implementation MyMusicPlayView

- (void)viewWillAppear:(BOOL)animated{
    MusicBean *bena = [appDele.m_arrMusic objectAtIndex:appDele.m_nSelIdx];
    if(bena.m_musicCover){
        self.roundView.roundImage = bena.m_musicCover;
        [self.roundView.roundImageView startTransitionAnimation];
        [self.backImageView setImage:bena.m_musicCover];
        [self.backImageView startTransitionAnimation];
    }else{
        self.roundView.roundImage = [UIImage imageNamed:@"play_songAlbumDefault"];
        [self.roundView.roundImageView startTransitionAnimation];
        [self.backImageView setImage:[UIImage imageNamed:@"bg_main"]];
        [self.backImageView startTransitionAnimation];
    }
    if(appDele.filePlayer && [appDele.filePlayer channelIsPlaying]){
        [self.roundView setIsPlay:YES];
        [_play setImage:[UIImage imageNamed:@"xmly_music_stop"] forState:UIControlStateNormal];
    }else{
        [self.roundView setIsPlay:NO];
        [_play setImage:[UIImage imageNamed:@"xmly_music_play"] forState:UIControlStateNormal];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if(timer){
        [timer invalidate];
        timer = nil;
    }
}

-(void)playMusicChange{
    //音乐播放改变
    //_m_musicBean = [appDele.m_arrMusic objectAtIndex:appDele.m_nSelIdx];
    
    if(appDele.playBean.m_musicCover){
        self.roundView.roundImage = appDele.playBean.m_musicCover;
        [self.roundView.roundImageView startTransitionAnimation];
        [self.backImageView setImage:appDele.playBean.m_musicCover];
        [self.backImageView startTransitionAnimation];
    }else{
        self.roundView.roundImage = [UIImage imageNamed:@"play_songAlbumDefault"];
        [self.roundView.roundImageView startTransitionAnimation];
        [self.backImageView setImage:[UIImage imageNamed:@"bg_main"]];
        [self.backImageView startTransitionAnimation];
    }
    self.end.text = appDele.playBean.m_musicTime;
    self.name.text = appDele.playBean.m_musicName;
    self.singername.text = appDele.playBean.m_singerName;
    if(appDele.filePlayer && [appDele.filePlayer channelIsPlaying]){
        [self.roundView setIsPlay:YES];
        [_play setImage:[UIImage imageNamed:@"xmly_music_stop"] forState:UIControlStateNormal];
        [_play startTransitionAnimation];
    }else{
        [self.roundView setIsPlay:NO];
        [_play setImage:[UIImage imageNamed:@"xmly_music_play"] forState:UIControlStateNormal];
        [_play startTransitionAnimation];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBlurView];   //设置图片模糊
    appDele = (AppDelegate *)[UIApplication sharedApplication].delegate;   //根对象
//    [self.slider setValue:0.0f];
    [self.slider setThumbImage:[UIImage imageNamed:@"music_slider_circle"]forState:UIControlStateNormal];
    self.slider.continuous = YES;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(popPlayProgress)  //刷新进度条
                                           userInfo:nil
                                            repeats:YES];
    if ([appDele.m_arrMusic count]!= 0 && appDele.m_nSelIdx!=-1) {
        //_m_musicBean = [appDele.m_arrMusic objectAtIndex:appDele.m_nSelIdx];
        self.name.text = appDele.playBean.m_musicName;
        self.singername.text = appDele.playBean.m_singerName;
        self.end.text = appDele.playBean.m_musicTime;
        if(appDele.filePlayer && [appDele.filePlayer channelIsPlaying]){
            CGFloat fCurrent=[appDele.filePlayer currentTime];
            [self.slider setValue:fCurrent/[appDele.filePlayer duration]];
            self.dur.text = [self strWithTimeInterval:appDele.filePlayer.currentTime];
        }
    }
    self.roundView.rotationDuration = 8.0;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPopup)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapRecognizer];
    UISwipeGestureRecognizer *recognizer;
    recognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(dismissPopup)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [[self view] addGestureRecognizer:recognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playMusicChange) name:@"playMusicChange" object:nil];
    
    //监听模式改变了
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissPopup) name:@"mainModeChangle" object:nil];
    //监听蓝牙断开了
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissPopup) name:@"disconnectionBluetooth" object:nil];
    
    [self changleRepeatImage];  //同步一下当前播放模式状态
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reSetAnimation) name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)reSetAnimation{
    if(appDele.filePlayer && [appDele.filePlayer channelIsPlaying]){
        [self.roundView setIsPlay:YES];
        [_play setImage:[UIImage imageNamed:@"xmly_music_stop"] forState:UIControlStateNormal];
    }else{
        [self.roundView setIsPlay:NO];
        [_play setImage:[UIImage imageNamed:@"xmly_music_play"] forState:UIControlStateNormal];
    }
}

- (void)setBlurView{
    UIToolbar *blurView = [[UIToolbar alloc] init];
    blurView.barStyle = UIBarStyleBlack;
    blurView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self.backImageView addSubview:blurView];
}

// 将秒转换为指定格式的字符串
- (NSString *)strWithTimeInterval:(NSTimeInterval)interval{
    int m = interval / 60;
    int s = (int)interval % 60;
    return [NSString stringWithFormat:@"%02d:%02d", m , s];
}

- (void)popPlayProgress{
    if([appDele.filePlayer channelIsPlaying]){
        CGFloat fCurrent=[appDele.filePlayer currentTime];
        [self.slider setValue:fCurrent/[appDele.filePlayer duration]];
        int seconds = (int)fCurrent;
        int minute = 0;
        if (seconds >= 60) {
            int index = seconds / 60;
            minute = index;
            seconds = seconds - index * 60;
        }
        self.dur.text = [NSString stringWithFormat:@"%02d:%02d", minute, seconds];
    }
}

- (void)dismissPopup{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)progress:(UISlider *)sender {
    if(appDele.filePlayer != nil && [appDele.filePlayer channelIsPlaying]){
        appDele.filePlayer.currentTime = (sender.value * [appDele.filePlayer duration]);
        self.dur.text = [self strWithTimeInterval:appDele.filePlayer.currentTime];
        //[appDele.filePlayer playAtTime:(self.slider.value * [appDele.filePlayer duration])];
        //[timer setFireDate:[NSDate date]];
    }
}
- (IBAction)next:(id)sender {
    if(appDele.m_arrMusic.count>0){
        [appDele playSong:MusicPlayStateModeNext];
    }
}

- (IBAction)play:(id)sender {
    if(appDele.filePlayer != nil && [appDele.filePlayer channelIsPlaying]){
        [_play setImage:[UIImage imageNamed:@"xmly_music_play"] forState:UIControlStateNormal];
        [appDele playSong:MusicPlayStateModePause];
    }else{
        [_play setImage:[UIImage imageNamed:@"xmly_music_stop"] forState:UIControlStateNormal];
        [appDele playSong:MusicPlayStateModePlay];
    }
    [_play startTransitionAnimation];
}
- (IBAction)pre:(id)sender {
    if(appDele.m_arrMusic.count>0){
        [appDele playSong:MusicPlayStateModePrevious];
    }
}
- (IBAction)sound_image_click:(id)sender {
    if(appDele.globalManager){//声音按钮
        if(!self.soundController){
            UIView *soundView = [[[NSBundle mainBundle] loadNibNamed:@"SoundPopView" owner:self options:nil] objectAtIndex:0];
            soundView.frame = CGRectMake(0, 0, self.view.frame.size.width, soundView.height);
            self.soundController = [[CNPPopupController alloc] initWithContents:@[soundView]];
            self.soundController.theme = [CNPPopupTheme defaultTheme];
            self.soundController.theme.popupStyle = CNPPopupStyleActionSheet;
            self.soundController.delegate = self;
            
            self.soundSliderView.minimumTrackTintColor = [UIColor whiteColor];
            self.soundSliderView.maximumTrackTintColor = [UIColor grayColor];
            [self.soundSliderView setThumbImage:[UIImage imageNamed:@"music_slider_circle"]forState:UIControlStateNormal];
            self.soundSliderView.maximumValue = appDele->maxVolue;
            self.soundSliderView.minimumValue = appDele->minVoloe;
        }
        self.soundPopLable.text = [NSString stringWithFormat:@"%@:%02d",NSLocalizedString(@"volume_text", nil), appDele->curr_volue];
        [self.soundSliderView setValue:appDele->curr_volue];
        [self.soundController presentPopupControllerAnimated:YES];
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"error_content_text", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"ok_text", nil) otherButtonTitles:nil, nil];
        [alertView show];   //显示对话框
    }
}

- (IBAction)radom_image_button:(id)sender {
    //随机播放按钮
    appDele.playRepeatMode += 1;
    if(appDele.playRepeatMode > 2){
        appDele.playRepeatMode = 0;
    }
    [self changleRepeatImage];
    
}

-(void)changleRepeatImage{
    if(appDele.playRepeatMode == 0){  //列表循环
        [self.repeatButton setImage:[UIImage imageNamed:@"loop_playback"] forState:UIControlStateNormal];
        [self showMiddleHint:NSLocalizedString(@"list_loop", nil)];
    }else if(appDele.playRepeatMode == 1){   //随机播放
        [self.repeatButton setImage:[UIImage imageNamed:@"random_play"] forState:UIControlStateNormal];
        [self showMiddleHint:NSLocalizedString(@"radom_play", nil)];
    }else if(appDele.playRepeatMode == 2){   //单曲循环
        [self.repeatButton setImage:[UIImage imageNamed:@"single_tune_circulation"] forState:UIControlStateNormal];
        [self showMiddleHint:NSLocalizedString(@"onlne_play", nil)];
    }
}

-(void)showMiddleHint:(NSString *)showContent{
    [self.view makeToast:showContent duration:2.0 position:CSToastPositionCenter];
}

- (IBAction)back_image_click:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)soundChangle:(id)sender {
    if(appDele.globalManager){
        [appDele.globalManager setVolume:self.soundSliderView.value];
        self.soundPopLable.text = [NSString stringWithFormat:@"%@:%02d",NSLocalizedString(@"volume_text", nil), appDele->curr_volue];
    }
}

@end
