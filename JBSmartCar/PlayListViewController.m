//
//  PlayListViewController.m
//  JBSmart_Car
//
//  Created by yangxiao on 16/7/9.
//  Copyright © 2016年 yangxiao. All rights reserved.
//

#import "PlayListViewController.h"
#import "XMSDK.h"
#import "MJRefresh.h"
#import "PlayListCell.h"
#import "PlayViewController.h"
#import "LocalPlayViewController.h"
#import "CarPlayViewController.h"

@interface PlayListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    int page;
    BOOL isbackplayView;
    
}
@end

@implementation PlayListViewController
-(void)viewWillDisappear:(BOOL)animated{
    
    [ProgressHUD dismiss];
}
-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDete=(AppDelegate*)[UIApplication sharedApplication].delegate;   //根对象
    self.titleLab.text=self.titleStr;
    self.listArr=[NSMutableArray new];
    self.titleLab.text=self.titleStr;
    self.playTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.playTableView registerNib:[UINib nibWithNibName:@"PlayListCell" bundle:nil] forCellReuseIdentifier:@"PlayListCell"];
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.playTableView.mj_footer = footer;
    page=1;
    
    [self loadMoreData];
   
}
-(void)loadMoreData{
    [ProgressHUD show:@"正在加载数据..."];
    
    if([self.playTableView.mj_footer isRefreshing]){
        ++page;
    }
    
    //上拉刷新更多
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.playID forKey:@"album_id"];
    [params setObject:@20 forKey:@"count"];
    [params setObject:[NSNumber numberWithInt:page] forKey:@"page"];
    [[XMReqMgr sharedInstance] requestXMData:XMReqType_AlbumsBrowse params:params withCompletionHander:^(id result, XMErrorModel *error) {
        if(!error){
            Class dataClass = NSClassFromString(@"XMTrack");
            if([result isKindOfClass:[NSDictionary class]]){
                NSDictionary *sondList = [result valueForKeyPath:@"tracks"];
                
                
                if(sondList.count < 20){
                    //数据全部加载完了
                    [self.playTableView.mj_footer endRefreshingWithNoMoreData];  //全部加载完毕
                }
                for (NSDictionary *dic in sondList) {
                    id model = [[dataClass alloc] initWithDictionary:dic];
                    [self.listArr addObject:model];
                }
                [self.playTableView reloadData];
            }
            [ProgressHUD dismiss];
        }else{
            if(page > 1){
                page = page-1;
            }
            [ProgressHUD showError:@"加载数据失败!"];
        }
        [self.playTableView.mj_footer endRefreshing];  //结束刷新
    }];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.listArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PlayListCell *cell=[tableView dequeueReusableCellWithIdentifier:@"PlayListCell"];
    id model=self.listArr[indexPath .row];
    NSString *titleStr=[model valueForKeyPath:@"trackTitle"];
    cell.titleLab.text=titleStr;
    NSString *subtitleStr=[model valueForKeyPath:@"trackTags"];
    if (subtitleStr!=nil) {
        
     cell.subtitleLab.text=subtitleStr;
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80.0f;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if ([appDete.m_play isPlaying]) {
        
        [appDete playSong:1];
    }
    
    if (appDete->isRemotePlaying  ||  appDete->curr_mode!=MODE_A2DP) {
        
      [appDete.musicManager pause];
      [appDete.globalManager setMode:MODE_A2DP];
        
    }
    appDete->isPlaying=YES;
   PlayViewController *vc =[PlayViewController new];
    vc.track=self.listArr[indexPath.row];
    vc.trackList=self.listArr;
    [self presentViewController:vc animated:YES completion:nil];

}

- (IBAction)backBtnCilck:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)playViewBtn:(id)sender {
    
    if (appDete->isPlaying && appDete->curr_mode==MODE_A2DP) {  //在线播放
        
        //显示正在播放的页面
        PlayViewController *plauVC = [[PlayViewController alloc] init];
      [self presentViewController:plauVC animated:YES completion:nil];
        
    }else if(appDete->hasCard && appDete->curr_mode==MODE_CARD){
        
        CarPlayViewController *vc=[CarPlayViewController new];
       [self presentViewController:vc animated:YES completion:nil];
        
    }else if (appDete->hasUPan && appDete->curr_mode==MODE_UHOST){
        
        CarPlayViewController *vc=[CarPlayViewController new];
      [self presentViewController:vc animated:YES completion:nil];
        
    }else{
        
        if (appDete.m_arrMusic.count==0) {
            
            return;
            
        }
        
        if (appDete->m_nSelIdx==-1) {   //本地播放
            
            appDete->m_nSelIdx=0;
            
        }
        
        LocalPlayViewController *vc=[LocalPlayViewController new];
 [self presentViewController:vc animated:YES completion:nil];
    }

    
}

@end
