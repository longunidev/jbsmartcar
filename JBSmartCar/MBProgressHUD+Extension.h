//
//  MBProgressHUD.h
//  NewBL
//
//  Created by 肖建明 on 16/9/20.
//  Copyright © 2016年 XJM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface MBProgressHUD (Extension)

+ (void)showHUD:(UIView *)view :(NSString *)title;

@end
