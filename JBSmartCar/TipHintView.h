//
//  TipHintView.h
//  ZXCS
//
//  Created by Apple on 17/7/7.
//  Copyright © 2017年 YANGXIAO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipHintView : UIView
{
    int count;
}
@property (weak, nonatomic) IBOutlet UILabel *hintLab;
@property (nonatomic,assign) BOOL isDestroy;   //消失后并退出死循环
@property (nonatomic,assign) BOOL firstTime;   //防止走马观灯调用多次
+(TipHintView *)tipHintManager;
+(instancetype)tipHintView;
+(void)showTipHint:(NSString *)tint;
+(void)hideTipHint;

@end
