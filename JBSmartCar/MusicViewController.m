//
//  MusicViewController.m
//  JBSmartCar
//
//  Created by xjm on 17/3/1.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import "MusicViewController.h"
#import "AppDelegate.h"
#import "MusicCell.h"
#import "MusicBean.h"
#import "EQSettingViewController.h"
#import "CNPPopupController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIView+Extension.h"
#import "MyMusicPlayView.h"
#import "ViewController.h"
#import "TipHintView.h"
#import "MarqueeLabel.h"
#import "TopAlert.h"

//检测当前系统版本的宏
#define IS_IOS10_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)

@interface MusicViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,MainDateValueChangleDelegate>{
    AppDelegate* appDele;   //根对象，获取蓝牙连接对象
    NSTimer *playTimer;    //定时更新当前播放进度Toobar
    
    
}
@property (weak, nonatomic) IBOutlet UITableView *musicTableview;
@property (weak, nonatomic) IBOutlet UILabel *musicNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIImageView *musicAlbumImg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *playLayoutBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *musicTableBotton;

@property (nonatomic, strong) CNPPopupController *soundController;
@property (weak, nonatomic) IBOutlet UISlider *soundSliderView;
@property (weak, nonatomic) IBOutlet UILabel *soundHintLable;

@property (weak, nonatomic) IBOutlet UIView *playToolsView;

@end

@implementation MusicViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //更改当前的模式
    if(appDele->curr_mode != MODE_A2DP && appDele.globalManager){
        [appDele.globalManager setMode:MODE_A2DP];
    }
    //刷新当前的播放状态
    if([self.musicTableview numberOfSections] != appDele.m_arrMusic.count){
        [self.musicTableview reloadData];  //喜马拉雅下载的被删除了
    }
    if(appDele.filePlayer && appDele.filePlayer.channelIsPlaying){
        [_playButton setImage:[UIImage imageNamed:@"music_stop"] forState:UIControlStateNormal];
    }else{
        [_playButton setImage:[UIImage imageNamed:@"music_play"] forState:UIControlStateNormal];
    }
    if(appDele.m_nSelIdx >= 0 && appDele.m_nSelIdx < appDele.m_arrMusic.count){
        [self.musicTableview selectRowAtIndexPath:[NSIndexPath indexPathForRow:appDele.m_nSelIdx inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];//设置选中列表
    }
    appDele->currentPage=3;
    if (appDele->isfirstA2dp) {
        [self refreshBluetooth];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    appDele->currentPage=0;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    appDele=(AppDelegate *)[UIApplication sharedApplication].delegate;   //根对象
    self.musicTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.musicTableview registerNib:[UINib nibWithNibName:@"MusicCell" bundle:nil] forCellReuseIdentifier:@"MusicCell"];
    self.musicTableview.rowHeight = 60.f;
    [self.musicTableview setDataSource:self];
    [self.musicTableview setDelegate:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playMusicChange) name:@"playMusicChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeVolumeChanged) name:@"homeVolumeChanged" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBluetooth) name:@"refreshBluetooth" object:nil];
  
    playTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updatePlayInfo) userInfo:nil repeats:YES];
    if(appDele.m_arrMusic.count == 0){
        if(IS_IOS10_OR_LATER && appDele.isMusicPermissionsDenied){   //读取音乐权限被拒绝
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"permiss_dine",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"cancel_text", nil) otherButtonTitles:nil, nil];
            [alertView show];
        }else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"into_music",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"ok_text", nil) otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    //状态栏添加手势
    UITapGestureRecognizer *stateGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showMusicStatePopView)];
    [self.playToolsView addGestureRecognizer:stateGesture];
    
    //设置底层的数据改变监听
    ViewController *parentView = (ViewController *)[self parentViewController];
    [parentView setValueDelegate:self];
}
-(void)refreshBluetooth{
    if(appDele.globalManager){
        int queKey = [appDele.globalManager buildKey:QUE cmdID:131];
        [appDele.globalManager sendCustomCommand:queKey param1:0 param2:0 others:nil];
    }
}
-(void)mainDateValueChangle:(int)date1 :(int)date2{
    int isContect = (int)(date2 >> 16);
    if (appDele->currentPage==3) {
        if (isContect==0) {
            if (appDele->isfirstA2dp) {
                appDele->isfirstA2dp=NO;
                TipHintView *tipView=[TipHintView tipHintManager];
                tipView.frame=CGRectMake(0, 64, self.view.frame.size.width, 20);
                [TipHintView showTipHint:NSLocalizedString(@"a2dpHint", nil)];
                [self.view addSubview:tipView];
            }
        }
    }
}

-(void)showMusicStatePopView{
    //显示音乐播放状态
    if(appDele.filePlayer){
        [self.navigationController presentViewController:[[MyMusicPlayView alloc] init] animated:YES completion:nil];
    }
}

-(void)updatePlayInfo{
    if(appDele.filePlayer && appDele.filePlayer.channelIsPlaying){
        if (NSClassFromString(@"MPNowPlayingInfoCenter")) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            [dict setObject:appDele.playBean.m_musicName forKey:MPMediaItemPropertyAlbumTitle];
            [dict setObject:appDele.playBean.m_singerName forKey:MPMediaItemPropertyArtist];
            [dict setObject:[NSNumber numberWithDouble:appDele.filePlayer.currentTime] forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime]; //音乐当前已经播放时间
            [dict setObject:[NSNumber numberWithFloat:1.0] forKey:MPNowPlayingInfoPropertyPlaybackRate]; //进度条光标的速度
            [dict setObject:[NSNumber numberWithDouble:appDele.filePlayer.duration] forKey:MPMediaItemPropertyPlaybackDuration]; //歌曲总时间设置
            MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc] initWithImage:appDele.playBean.m_musicCover ? appDele.playBean.m_musicCover:appDele.defautImage];
            [dict setObject:artwork forKey:MPMediaItemPropertyArtwork];
            [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:dict];
        }
    }
}

-(void)playMusicChange{
    if(appDele.m_nSelIdx >= 0 && appDele.m_nSelIdx < appDele.m_arrMusic.count){
        [self.musicTableview selectRowAtIndexPath:[NSIndexPath indexPathForRow:appDele.m_nSelIdx inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];//设置选中列表
    }
    if(appDele.playBean){
        
        self.musicNameLabel.text = appDele.playBean.m_musicName;
        if(appDele.playBean.m_musicCover==nil){
        
            self.musicAlbumImg.image=[UIImage imageNamed:@"ic_album_cover_default"];
            
        }else{
        
        self.musicAlbumImg.image = appDele.playBean.m_musicCover;
            
        }
        
        
    }
    if(appDele.filePlayer && appDele.filePlayer.channelIsPlaying){
        [_playButton setImage:[UIImage imageNamed:@"music_stop"] forState:UIControlStateNormal];
    }else{  //
        [_playButton setImage:[UIImage imageNamed:@"music_play"] forState:UIControlStateNormal];
    }
    if(self.playLayoutBottom.constant != 49){
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:0.8 animations:^{
            [self.playLayoutBottom setConstant:49.f];
            [self.musicTableBotton setConstant:104.f]; //49 + 55
            [self.view layoutIfNeeded]; // Called on parent view
        }];
    }
}

- (IBAction)eqSettingButtonClick:(id)sender {
    if(appDele.globalManager){
        EQSettingViewController *eqSettingVC = [[EQSettingViewController alloc] init];
        [self.navigationController pushViewController:eqSettingVC animated:YES];
    }else{
        [self showConnectionDialog];
    }
}

-(void)homeVolumeChanged{
    if(appDele.globalManager && self.soundController){
        [self.soundSliderView setValue:appDele->curr_volue];
        self.soundHintLable.text = [NSString stringWithFormat:@"%@:%i", NSLocalizedString(@"volume_text",nil) ,appDele->curr_volue];
    }
}

//音量大小控制
- (IBAction)soundButtonClick:(id)sender {
    if(appDele.globalManager){
        if(!self.soundController){
            UIView *soundView = [[[NSBundle mainBundle] loadNibNamed:@"CardSoundPopView" owner:self options:nil] objectAtIndex:0];
            soundView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, soundView.height);
            self.soundSliderView.minimumTrackTintColor = [UIColor whiteColor];
            self.soundSliderView.maximumTrackTintColor = [UIColor grayColor];
            [self.soundSliderView setThumbImage:[UIImage imageNamed:@"music_slider_circle"]forState:UIControlStateNormal];
            
            self.soundController = [[CNPPopupController alloc] initWithContents:@[soundView]];
            self.soundController.theme = [CNPPopupTheme defaultTheme];
            self.soundController.theme.popupStyle = CNPPopupStyleActionSheet;
        }
        [self.soundSliderView setMinimumValue:appDele->minVoloe];
        [self.soundSliderView setMaximumValue:appDele->maxVolue];
        [self.soundSliderView setValue:appDele->curr_volue];
        self.soundHintLable.text = [NSString stringWithFormat:@"%@:%i", NSLocalizedString(@"volume_text",nil) ,appDele->curr_volue];
        [self.soundController presentPopupControllerAnimated:YES];
    }else{
        [self showConnectionDialog];
    }
}
-(void)showConnectionDialog{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"error_content_text", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"ok_text", nil) otherButtonTitles:nil, nil];
    [alertView show];   //显示对话框
}
- (IBAction)soundSeekBarChange:(UISlider *)sender {
    if(appDele.globalManager){
        [appDele.globalManager setVolume:(int)sender.value];
        self.soundHintLable.text = [NSString stringWithFormat:@"%@:%i", NSLocalizedString(@"volume_text",nil) ,(int)sender.value];
    }
}

//多少项目
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [appDele.m_arrMusic count];   //返回当前音乐信息列表长度
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger nRow=[indexPath row];
    MusicCell*cell = [tableView dequeueReusableCellWithIdentifier:@"MusicCell"];
    cell.layer.transform=CATransform3DMakeScale(0.1, 0.1, 1);
    [UIView animateWithDuration:0.5 animations:^{
        cell.layer.transform=CATransform3DMakeScale(1, 1, 1);
    }];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    MusicBean * bean = [appDele.m_arrMusic objectAtIndex:nRow];
    cell.musicName.text=bean.m_musicName;
    cell.singName.text=bean.m_singerName;
    cell.timeLab.text=bean.m_musicTime;
    cell.numLab.text=[NSString stringWithFormat:@"%ld",(long)nRow+1];
    return cell;
}

//下表选中
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self checkCurrIsMute];
    if(appDele.m_nSelIdx != indexPath.row){
        appDele.m_nSelIdx = (int)indexPath.row;
        [appDele playSong:MusicPlayStateModeNew];
    }else{
        if(appDele.filePlayer && appDele.filePlayer.channelIsPlaying){
            [appDele playSong:MusicPlayStateModePause];
        }else{
            [appDele playSong:MusicPlayStateModePlay];
        }
    }
}
//播放按钮被点击
- (IBAction)playButtonClick:(id)sender {
    if(appDele.filePlayer && appDele.filePlayer.channelIsPlaying){
        [appDele playSong:MusicPlayStateModePause];
    }else{
        [self checkCurrIsMute];
        [appDele playSong:MusicPlayStateModePlay];
    }
}
//下一曲音乐
- (IBAction)nextButtonClick:(id)sender {
    [self checkCurrIsMute];
    [appDele playSong:MusicPlayStateModeNext];
}

-(void)checkCurrIsMute{
    if(appDele.globalManager && appDele->isMute){
        UIAlertView *hintAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"mute_hint", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel_text", nil) otherButtonTitles:NSLocalizedString(@"ok_text", nil), nil];
        hintAlert.tag = 101;
        [hintAlert show];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 101 && buttonIndex == 1){
        [appDele.globalManager switchMute];
    }
}

@end
