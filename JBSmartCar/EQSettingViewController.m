//
//  EQSettingViewController.m
//  JBSmartCar
//
//  Created by xjm on 17/3/20.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import "EQSettingViewController.h"
#import "AppDelegate.h"

@interface EQSettingViewController (){
    AppDelegate* appDele;   //根对象，获取蓝牙连接对象
}

@property (weak, nonatomic) IBOutlet UISlider *slider_32;
@property (weak, nonatomic) IBOutlet UISlider *slider_64;
@property (weak, nonatomic) IBOutlet UISlider *slider_125;
@property (weak, nonatomic) IBOutlet UISlider *slider_250;
@property (weak, nonatomic) IBOutlet UISlider *slider_500;
@property (weak, nonatomic) IBOutlet UISlider *slider_1000;
@property (weak, nonatomic) IBOutlet UISlider *slider_2000;
@property (weak, nonatomic) IBOutlet UISlider *slider_4000;
@property (weak, nonatomic) IBOutlet UISlider *slider_8000;
@property (weak, nonatomic) IBOutlet UISlider *slider_16000;

@end

@implementation EQSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDele= (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    UIImage *eqSliderImage = [UIImage imageNamed:@"music_slider_circle"];
    [self.slider_16000 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_8000 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_4000 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_2000 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_1000 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_500 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_250 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_125 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_64 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    [self.slider_32 setThumbImage:eqSliderImage forState:UIControlStateNormal];
    
    //设置当前的EQ值
    for (AEParametricEqFilter *existEqFilter in appDele.eqFilters) {
        if (existEqFilter.centerFrequency == 16000) {
            [self.slider_16000 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 8000){
            [self.slider_8000 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 4000){
            [self.slider_4000 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 2000){
            [self.slider_2000 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 1000){
            [self.slider_1000 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 500){
            [self.slider_500 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 250){
            [self.slider_250 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 125){
            [self.slider_125 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 64){
            [self.slider_64 setValue:existEqFilter.gain animated:YES];
        }else if(existEqFilter.centerFrequency == 32){
            [self.slider_32 setValue:existEqFilter.gain animated:YES];
        }
    }
}
- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)eqSliderChange:(UISlider *)sender {
    switch (sender.tag) {
        case 16000:
            [appDele setupEqFilter:appDele.eq16KHzFilter centerFrequency:16000 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider16K"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 8000:
            [appDele setupEqFilter:appDele.eq8KHzFilter centerFrequency:8000 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider8K"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 4000:
            [appDele setupEqFilter:appDele.eq4KHzFilter centerFrequency:4000 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider4K"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 2000:
            [appDele setupEqFilter:appDele.eq2KHzFilter centerFrequency:2000 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider2K"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 1000:
            [appDele setupEqFilter:appDele.eq1KHzFilter centerFrequency:1000 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider1K"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 500:
            [appDele setupEqFilter:appDele.eq500HzFilter centerFrequency:500 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider500"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 250:
            [appDele setupEqFilter:appDele.eq250HzFilter centerFrequency:250 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider250"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 125:
            [appDele setupEqFilter:appDele.eq125HzFilter centerFrequency:125 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider125"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 64:
            [appDele setupEqFilter:appDele.eq64HzFilter centerFrequency:64 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider64"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        case 32:
            [appDele setupEqFilter:appDele.eq32HzFilter centerFrequency:32 gain:sender.value];
            [[NSUserDefaults standardUserDefaults] setFloat:sender.value forKey:@"slider32"];
            [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
            break;
        default:
            break;
    }
}

- (IBAction)refreshButtonClick:(id)sender {
    [self eqRestoreButtonClick];
}

-(void)eqRestoreButtonClick{
    [self.slider_16000 setValue:0 animated:YES];
    [self.slider_8000 setValue:0 animated:YES];
    [self.slider_4000 setValue:0 animated:YES];
    [self.slider_2000 setValue:0 animated:YES];
    [self.slider_1000 setValue:0 animated:YES];
    [self.slider_500 setValue:0 animated:YES];
    [self.slider_250 setValue:0 animated:YES];
    [self.slider_125 setValue:0 animated:YES];
    [self.slider_64 setValue:0 animated:YES];
    [self.slider_32 setValue:0 animated:YES];
    //记住本地数据
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider16K"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider8K"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider4K"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider2K"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider1K"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider500"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider250"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider125"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider64"];
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"slider32"];
    [[NSUserDefaults standardUserDefaults] synchronize];  //进行同步数据
    //设置本地音效
    [appDele setupEqFilter:appDele.eq16KHzFilter centerFrequency:16000 gain:0];
    [appDele setupEqFilter:appDele.eq8KHzFilter centerFrequency:8000 gain:0];
    [appDele setupEqFilter:appDele.eq4KHzFilter centerFrequency:4000 gain:0];
    [appDele setupEqFilter:appDele.eq2KHzFilter centerFrequency:2000 gain:0];
    [appDele setupEqFilter:appDele.eq1KHzFilter centerFrequency:1000 gain:0];
    [appDele setupEqFilter:appDele.eq500HzFilter centerFrequency:500 gain:0];
    [appDele setupEqFilter:appDele.eq250HzFilter centerFrequency:250 gain:0];
    [appDele setupEqFilter:appDele.eq125HzFilter centerFrequency:125 gain:0];
    [appDele setupEqFilter:appDele.eq64HzFilter centerFrequency:64 gain:0];
    [appDele setupEqFilter:appDele.eq32HzFilter centerFrequency:32 gain:0];
}

@end
