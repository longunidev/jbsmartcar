//
//  TipHintView.m
//  ZXCS
//
//  Created by Apple on 17/7/7.
//  Copyright © 2017年 YANGXIAO. All rights reserved.
//
#define kWidth [UIScreen mainScreen].bounds.size.width
#import "TipHintView.h"


@implementation TipHintView


+(TipHintView *)tipHintManager{

    static TipHintView *instance=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance=[TipHintView tipHintView];
        
    });
    
    return instance;

}

+(instancetype)tipHintView{

  return [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil] lastObject];
}

+(void)showTipHint:(NSString *)hint{

    [[self tipHintManager] tipHintNsstring:hint];

}
-(void)tipHintNsstring:(NSString *)hintStr{

    self.isDestroy=YES;
    self.hintLab.text=hintStr;
    [self animationChannelTitleLabel:self.hintLab isChange:YES];
    
}
-(void)animationChannelTitleLabel:(UILabel *)titleLab isChange:(BOOL)isChange{
    
    
    if (isChange) {
        
        CGRect frame=titleLab.frame;
        TipHintView *view=[TipHintView tipHintView];
        frame.origin.x=view.frame.size.width;
        count++;
        titleLab.frame=frame;
        CGFloat width=[self widthOfString:self.hintLab.text font:self.hintLab.font height:20.0];
        //计算动画时长
        CGFloat duration=(width + view.frame.size.width) * 0.02;
        [UIView animateWithDuration:duration animations:^{
            
            CGRect frame = titleLab.frame;
            frame.origin.x=(-1)*width;
            titleLab.frame=frame;
            
        } completion:^(BOOL finished) {
    
            if (count==3) {
                
            self.isDestroy=NO;
            count=0;
                
            }
            
            [self animationChannelTitleLabel:titleLab isChange:self.isDestroy];
            
        }];
        
    }
    
}
//计算文字宽度
-(CGFloat)widthOfString:(NSString *)string font:(UIFont *)font height:(CGFloat)height
{
    NSDictionary * dict=[NSDictionary dictionaryWithObject: font forKey:NSFontAttributeName];
    CGRect rect=[string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, height) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    return rect.size.width;
}

+(void)hideTipHint{

    [[self tipHintManager] dismiss];

}
-(void)dismiss{

    [self removeFromSuperview];
    self.isDestroy=NO;
}
@end
