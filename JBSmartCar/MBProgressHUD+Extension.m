//
//  MBProgressHUD.m
//  NewBL
//
//  Created by 肖建明 on 16/9/20.
//  Copyright © 2016年 XJM. All rights reserved.
//

#import "MBProgressHUD+Extension.h"

@implementation MBProgressHUD (Extension)

+ (void)showHUD:(UIView *)view :(NSString *)title{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    [hud.bezelView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.3]];
    [hud.bezelView setAlpha:0.4];
    hud.contentColor = [UIColor whiteColor];
    hud.label.text = title;
}

@end
