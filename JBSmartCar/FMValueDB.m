//
//  FMValueDB.m
//  ShiJie
//
//  Created by 肖建明 on 16/3/16.
//  Copyright © 2016年 Veryed. All rights reserved.
//

#import "FMValueDB.h"

#define DBNAME          @"fm.sqlite"

@implementation FMValueDB

-(BOOL)opneDatabase{    //打开数据库
    NSArray *Paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [Paths objectAtIndex:0];   //获取数据库路径
    NSString *databaseFilePath = [documentDirectory stringByAppendingPathComponent:DBNAME];
    if(sqlite3_open([databaseFilePath UTF8String], &database) != SQLITE_OK){   //自动创建一个数据库
        sqlite3_close(database);
        return NO;
    }
    NSString *createSQL = @"CREATE TABLE IF NOT EXISTS fmList(id INTEGER PRIMARY KEY AUTOINCREMENT, fmVolue INTEGER);";
    char *errorMsg;    //错误码
    if (sqlite3_exec(database, [createSQL UTF8String], NULL, NULL, &errorMsg) != SQLITE_OK){
        sqlite3_close(database);
        return NO;
    }
    return YES;
}
-(void)closeDB{
    //关闭数据库
    if(database != nil){
        sqlite3_close(database);
    }
}
-(NSMutableArray *)getAllFM{   //获取所有的音乐
    NSMutableArray *msuicList = [[NSMutableArray alloc] init];
    char *selectSql = "select fmVolue from fmList";
    sqlite3_stmt *statement;   //结果集
    int success = sqlite3_prepare_v2(database, selectSql, -1, &statement, nil);
    if (success==SQLITE_OK){
        while (sqlite3_step(statement) == SQLITE_ROW){//SQLITE_OK SQLITE_ROW{
            int index=sqlite3_column_int(statement, 0);
            [msuicList addObject:[NSNumber numberWithInt:index]];
        }
    }
    sqlite3_finalize(statement);   //释放结果集
    return msuicList;
}
-(BOOL)addFMValue:(int)fmValue{
    sqlite3_stmt *stmt;
    //插入数据
    char *update = "INSERT INTO fmList (fmVolue) VALUES (?);";
    if (sqlite3_prepare_v2(database, update, -1, &stmt, nil) == SQLITE_OK) {
        sqlite3_bind_int(stmt, 1, fmValue);
    }
    if (sqlite3_step(stmt) != SQLITE_DONE){
        NSLog(@"更新数据库表FIELDS出错");
    }
    sqlite3_finalize(stmt);   //提交数据
    return YES;
}
-(BOOL)deleteFmValue:(int)fmValue
{
    sqlite3_stmt *stmt;
    char *sql = "delete from fmList  where fmVolue = ?";
    int success = sqlite3_prepare_v2(database, sql, -1, &stmt, NULL);
    if (success != SQLITE_OK) {
        NSLog(@"删除数据出错");
        return NO;
    }
    sqlite3_bind_int(stmt, 1, fmValue);
    success = sqlite3_step(stmt);//提交数据
    sqlite3_finalize(stmt);//释放资源
    //如果执行失败
    if (success == SQLITE_ERROR) {
        NSLog(@"Error: failed to delete the database with message.");
        //关闭数据库
        return NO;
    }
    return YES;
}


@end
