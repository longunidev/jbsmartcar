//
//  MenuViewController.m
//  JBSmart_Car
//
//  Created by yangxiao on 16/7/9.
//  Copyright © 2016年 yangxiao. All rights reserved.
//

#import "MenuViewController.h"
#import "MeunCell.h"
#import "PlayListViewController.h"
#import "PlayViewController.h"
#import "LocalPlayViewController.h"
#import "CarPlayViewController.h"
@interface MenuViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    int curr_Pager;   //当前页面
    
}
@end

@implementation MenuViewController
-(void)viewWillDisappear:(BOOL)animated{
    
    [ProgressHUD dismiss];
}
-(void)viewDidAppear:(BOOL)animated{
    
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDete=(AppDelegate*)[UIApplication sharedApplication].delegate;   //根对象
    
     curr_Pager = 1;
    [self initTableView];
    [self loadDatas];
    [self initMJRefresh];
   
}
-(void)initTableView{
    
    self.titleLab.text=self.categoteName;
    self.datas=[NSMutableArray new];
    [self.menuTableview registerNib:[UINib nibWithNibName:@"MeunCell" bundle:nil] forCellReuseIdentifier:@"MeunCell"];
    self.menuTableview.separatorStyle = UITableViewCellSelectionStyleNone;

}
-(void)loadDatas{
    
    
    if ([self.menuTableview.mj_footer isRefreshing]) {
        
    }else if ([self.menuTableview.mj_header isRefreshing]){
        
        if(self.datas.count==0){
            
            curr_Pager=1;
        }else{
            
            [self performSelector:@selector(cancelRefresh) withObject:nil afterDelay:2.0f];
            return;
        }
        
    }else{
        
        curr_Pager=1;
    }
    [ProgressHUD show:NSLocalizedString(@"Is_trying", nil)];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@20 forKey:@"count"];
    [params setObject:[NSNumber numberWithInt:curr_Pager] forKey:@"page"];
    [params setObject:self.categoteID forKey:@"category_id"];
    [[XMReqMgr sharedInstance] requestXMData:XMReqType_AlbumsHot params:params withCompletionHander:^(id result, XMErrorModel *error) {
        
        if(!error){
            if([result isKindOfClass:[NSDictionary class]]){
                if([self.menuTableview.mj_footer isRefreshing]){
                    //正在加载更多
                }else{
                    [self.datas removeAllObjects];
                }
                Class dataClass = NSClassFromString(@"XMAlbum");
                NSDictionary *albumList = [result valueForKeyPath:@"albums"];
                
                for(NSDictionary *dct in albumList){
                    id model = [[dataClass alloc] initWithDictionary:dct];
                    [self.datas addObject:model];
                    
                }
                [self.menuTableview reloadData];
                [ProgressHUD dismiss];
            }
        }else{
            [ProgressHUD showError:NSLocalizedString(@"Failed_to_load_data", nil)];
            [self.navigationController popViewControllerAnimated:YES];
        }
        [self cancelRefresh];
    }];
    
}
-(void)cancelRefresh{
    [self.menuTableview.mj_header endRefreshing];
    [self.menuTableview.mj_footer endRefreshing];  //结束当前刷新状态
}
-(void)initMJRefresh{

    //设置上拉刷新
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.menuTableview.mj_footer = footer;
    //设置下拉刷新
    // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的loadNewData方法）
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(pushModeData)];
    // 隐藏时间
    header.lastUpdatedTimeLabel.hidden = YES;
    // 设置文字
    [header setTitle:NSLocalizedString(@"Drop_refresh_data", nil) forState:MJRefreshStateIdle];
    [header setTitle:NSLocalizedString(@"Release_start_refresh", nil) forState:MJRefreshStatePulling];
    [header setTitle:NSLocalizedString(@"Connecting_data", nil) forState:MJRefreshStateRefreshing];
    // 设置header
    self.menuTableview.mj_header = header;

}
-(void)loadMoreData{
    
    curr_Pager++;
    [self loadDatas];
    
}
-(void)pushModeData{
    
    [self loadDatas];
}
#pragma mark--------------tableview----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.datas.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MeunCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MeunCell"];
    id model = [self.datas objectAtIndex:indexPath.row];
    if([model isKindOfClass:[XMAlbum class]]){
        cell.titleLab.text = [model valueForKeyPath:@"albumTitle"];
   cell.numLab.text=[NSString stringWithFormat:@"节目数: %@万",[model valueForKeyPath:@"includeTrackCount"]];        NSString *playCountStr=[model valueForKeyPath:@"playCount"];
        CGFloat playcount=[playCountStr floatValue];
 
        cell.playcountLab.text=[NSString stringWithFormat:@"总播放量: %.2f万",playcount/10000];
        [cell.iconImg sd_setImageWithURL:[NSURL URLWithString:[model valueForKeyPath:@"coverUrlMiddle"]]];
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80.0;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id model = [self.datas objectAtIndex:indexPath.row];
    NSString *categoryId = [model valueForKeyPath:@"albumId"];
    PlayListViewController *vc=[PlayListViewController new];
    NSString *titleStr=[model valueForKeyPath:@"albumTitle"];
    vc.titleStr=titleStr;
    vc.playID=[NSNumber numberWithInt:[categoryId intValue]];
    
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (IBAction)backBtnCilck:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)playViewBtn:(id)sender {
    
    if (appDete->isPlaying && appDete->curr_mode==MODE_A2DP) {  //在线播放
        
        //显示正在播放的页面
        PlayViewController *plauVC = [[PlayViewController alloc] init];
        [self presentViewController:plauVC animated:YES completion:nil];
        
    }else if(appDete->hasCard && appDete->curr_mode==MODE_CARD){
        
        CarPlayViewController *vc=[CarPlayViewController new];
      [self presentViewController:vc animated:YES completion:nil];
        
    }else if (appDete->hasUPan && appDete->curr_mode==MODE_UHOST){
        
        CarPlayViewController *vc=[CarPlayViewController new];
        [self presentViewController:vc animated:YES completion:nil];
    }else{
        
        if (appDete.m_arrMusic.count==0) {
            
            return;
            
        }
        
        if (appDete->m_nSelIdx==-1) {   //本地播放
            
            appDete->m_nSelIdx=0;
            
        }
        
        LocalPlayViewController *vc=[LocalPlayViewController new];
       [self presentViewController:vc animated:YES completion:nil];
    }

    
}


@end
