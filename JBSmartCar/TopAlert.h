//
//  TopAlert.h
//  TestAlertTopView
//
//  Created by 肖建明 on 2016/12/6.
//  Copyright © 2016年 Veryed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"
@interface TopAlert : UIView
- (instancetype)initWithStyle:(NSString *)contentStr;
-(void)showInfo;
@end
