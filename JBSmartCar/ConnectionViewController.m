//
//  ConnectionViewController.m
//  JBSmartCar
//
//  Created by xjm on 17/3/1.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import "ConnectionViewController.h"
#import "VCDeviceCell.h"
#import "MJRefresh.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"   //Toast
#import "CNPPopupController.h"
#import "PulsingHaloLayer.h"
#import "HelpViewController.h"

@interface ConnectionViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,CBCentralManagerDelegate>{
    AppDelegate *appDele;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) UIAlertView *outTimeAlertView;
@property (weak, nonatomic) IBOutlet UIButton *scanButton;

//蓝牙管理对象
@property (nonatomic ,strong) CBCentralManager *cmg;
@property (nonatomic, strong) PulsingHaloLayer *halo;

@end

@implementation ConnectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    appDele = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.tableView.rowHeight = 55.f;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadDate)];
    // 隐藏时间
    header.lastUpdatedTimeLabel.hidden = YES;
    // 设置文字
    [header setTitle:NSLocalizedString(@"pull_to_refresh", nil) forState:MJRefreshStateIdle];
    [header setTitle:NSLocalizedString(@"pull_to_refresh", nil) forState:MJRefreshStatePulling];
    [header setTitle:NSLocalizedString(@"pull_to_refresh", nil) forState:MJRefreshStateRefreshing];
    header.stateLabel.textColor=[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0];
    // 设置header
    self.tableView.mj_header = header;
    self.tableView.separatorColor = [UIColor colorWithWhite:0 alpha:0.2];
    UINib *nib = [UINib nibWithNibName:@"VCDeviceCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"tabCell"];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    //注册接收通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshBluetooth) name:@"refreshBluetooth" object:nil];
    //蓝牙状态监控
    self.cmg = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    
    [self.scanButton.layer setCornerRadius:35.0f];
    [self.scanButton.layer setBorderWidth:1.0f];
    UIColor *layerColor = [UIColor colorWithRed:1.0 green:0.5 blue:0 alpha:0.3];
    [self.scanButton.layer setBorderColor:layerColor.CGColor];
    
    self.halo = [PulsingHaloLayer layer];
    self.halo.haloLayerNumber = 5;
    self.halo.radius = 150;
    self.halo.fromValueForRadius = 0.35;
    self.halo.animationDuration = 8;
    [self.halo setBackgroundColor:layerColor.CGColor];
    [self.scanButton.superview.layer insertSublayer:self.halo below:self.scanButton.layer];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.halo.position = self.scanButton.center;   //适配iPhone6
}

- (IBAction)helpButtonClick:(id)sender {
    [self.navigationController pushViewController:[[HelpViewController alloc] init] animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.view hideToastActivity];
    [self.navigationController.view hideToastActivity];  //隐藏
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    if(central.state == CBCentralManagerStatePoweredOn){
        //蓝牙开启状态
        appDele.isOpenBlue = YES;
        if(!appDele.globalManager && ![self.tableView.mj_header isRefreshing]){
            //第一次显示正在搜索对话框
            [self.navigationController.view makeToastActivity:CSToastPositionCenter];
            [self.tableView.mj_header beginRefreshing];
        }
        
    }else if(central.state == CBCentralManagerStatePoweredOff){
        appDele.isOpenBlue = NO;
        [appDele.mBluzConnector scanStop];
        [self.tableView.mj_header endRefreshing];
        [self.navigationController.view hideToastActivity];  //隐藏
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(void)showOpenBluetoothDialog{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"open_bluetooth", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"ok_text", nil) otherButtonTitles:nil, nil];
    [alertView show];  //显示对话框
}

//下拉刷新
-(void)loadDate{
    if(!appDele.isOpenBlue){
        [self showOpenBluetoothDialog];
        //结束搜索状态
        [self.tableView.mj_header endRefreshing];
        //隐藏对话框
        [self.navigationController.view hideToastActivity];
        return;
    }
    if(appDele.connCBPeripheral != nil && appDele.globalManager != nil){
        //当前蓝牙已经连接了
        [self.tableView.mj_header endRefreshing];
        //隐藏对话框
        [self.navigationController.view hideToastActivity];
    }else{
        [appDele.bluetoothDic removeAllObjects];
        //进行刷新列表
        [self.tableView reloadData];
        [appDele.mBluzConnector scanStart];
        if(!self.halo.isHaloAnimationing){
            [self.halo start];          //开始脉冲动画
         }
        [self.navigationController.view makeToastActivity:CSToastPositionCenter];
        //开始进行计时
        __weak typeof (self)weakself = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(appDele.bluetoothDic.count == 0 && !appDele.globalManager){
                if(!weakself.outTimeAlertView || ![weakself.outTimeAlertView isVisible]){
                    weakself.outTimeAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"scan_timeout",nil) message:NSLocalizedString(@"scan_timeout_content",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"ok_text",nil) otherButtonTitles:nil, nil];
                    [weakself.outTimeAlertView show];
                    [weakself.tableView.mj_header endRefreshing];
                    [weakself.navigationController.view hideToastActivity];
                }
            }
        });
    }
}

-(void)refreshBluetooth{
    
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
    if(appDele.globalManager){
        
        if(self.halo.isHaloAnimationing){
            
            [self.halo stop];
        }
        [self.navigationController.view hideToastActivity];
    }
    //关闭超时对话框
    if(appDele.bluetoothDic.count > 0 && [self.outTimeAlertView isVisible]){
        [self.outTimeAlertView dismissWithClickedButtonIndex:0 animated:YES];
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return appDele.bluetoothDic.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    VCDeviceCell* VDC=[tableView dequeueReusableCellWithIdentifier:@"tabCell"];
    VDC.selectionStyle = UITableViewCellSelectionStyleNone;
    NSMutableDictionary* dict = [appDele.bluetoothDic objectAtIndex:indexPath.row];
    CBPeripheral *cb = [dict objectForKey:@"peripheral"];
    NSString *name = [dict objectForKey:@"name"];
    if(name){
        VDC.DeviceTitle.text = name;
    }else{
        VDC.DeviceTitle.text = cb.name;
    }
    if (cb.state == CBPeripheralStateConnected) {
        VDC.DeviceState.text=NSLocalizedString(@"Connected", nil);
        [VDC.DeviceTitle setTextColor:[UIColor orangeColor]];
        [VDC.DeviceState setTextColor:[UIColor orangeColor]];
    }else{
        VDC.DeviceState.text=NSLocalizedString(@"Disconnect", nil);
        [VDC.DeviceTitle setTextColor:[UIColor whiteColor]];
        [VDC.DeviceState setTextColor:[UIColor whiteColor]];
    }
    VDC.backgroundColor = [UIColor clearColor];
    return VDC;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableDictionary* dict = [appDele.bluetoothDic objectAtIndex:indexPath.row];
    CBPeripheral *cb = [dict objectForKey:@"peripheral"];
    appDele.autoConnManuter = [dict objectForKey:@"manufacturerStr"];
    //你确定断开当前蓝牙吗?
    if(appDele.globalManager && cb.state == CBPeripheralStateConnected){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"is_disconnection", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel_text", nil) otherButtonTitles:NSLocalizedString(@"ok_text", nil), nil];
        alertView.tag = 101;
        [alertView show];  //显示对话框
        return;
    }else{
        //蓝牙正在连接...
        appDele.connCBPeripheral = cb;
        [appDele.mBluzConnector connect:cb];
        [self.navigationController.view makeToastActivity:CSToastPositionCenter];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 101 && buttonIndex==1){
         //用户主动断开蓝牙
        if(appDele.connCBPeripheral){
            appDele.autoConnManuter = nil;
            [appDele.mBluzConnector disconnect:appDele.connCBPeripheral];//断开当前的蓝牙连接
        }
    }
}
- (IBAction)searchBtn:(id)sender {
    if(appDele.connCBPeripheral != nil && appDele.mMediaManager != nil){
        //当前蓝牙已经连接了
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text", nil) message:NSLocalizedString(@"is_disconnection", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"cancel_text",nil) otherButtonTitles:NSLocalizedString(@"ok_text", nil) , nil];
        alertView.tag = 101;
        [alertView show];  //显示对话框
        return;
    }else{
        [self.tableView.mj_header beginRefreshing];
    }
}

@end
