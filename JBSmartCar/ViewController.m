//
//  ViewController.m
//  JBSmartCar
//
//  Created by xjm on 17/3/1.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"   //Toast
#import "CardViewController.h"
#import "TipHintView.h"
#import <MediaPlayer/MediaPlayer.h>


@interface ViewController ()<ConnectDelegate,GlobalDelegate,UIAlertViewDelegate>{
    AppDelegate *appDele;
    
    BOOL ishint;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //开始进行初始化数据连接
    appDele = (AppDelegate *)[UIApplication sharedApplication].delegate;
       //初始化蓝牙设备
    appDele.mBluzConnector = [[BluzDevice alloc] init];
    [appDele.mBluzConnector setConnectDelegate:self];    //设置连接代理
    [appDele.mBluzConnector setAppForeground:YES];   //设置前台
    
    //获取到自动连接的特征码
    NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
    appDele.autoConnManuter = [userDefaultes stringForKey:@"manufacturerStr"];  //自动回连的特征码
    
    //屏蔽系统的音量弹出框
    UISlider *slider =[self getSystemVolumSlider];
    slider.frame = CGRectMake(-200, -200, 10, 10);
    [self.view addSubview:slider];
}

//获取系统音量滑块
-(UISlider*)getSystemVolumSlider{
    static UISlider * volumeViewSlider = nil;
    if (volumeViewSlider == nil) {
        MPVolumeView *volumeView = [[MPVolumeView alloc] initWithFrame:CGRectMake(10, 50, 200, 4)];
        for (UIView* newView in volumeView.subviews) {
            if ([newView.class.description isEqualToString:@"MPVolumeSlider"]){
                volumeViewSlider = (UISlider*)newView;
                break;
            }
        }
    }
    return volumeViewSlider;
}

//蓝牙断开了
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag == 100){   //只有一个按钮 蓝牙断开了
        //通知蓝牙进行搜索
    }else if(alertView.tag == 102){
        if(buttonIndex == 0){
            [self goSystemPager];
        }
    }
}
-(void)goSystemPager{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 10) {
        NSURL *url = [NSURL URLWithString:@"prefs:root=Bluetooth"];
        if ([[UIApplication sharedApplication]canOpenURL:url]) {
            [[UIApplication sharedApplication]openURL:url];
        }
    }
}

-(void)modeChanged:(UInt32)mode{
    appDele->curr_mode = mode;
    [self.navigationController.view hideToastActivity];
    [self.navigationController popViewControllerAnimated:YES];
    if(mode == MODE_CARD){
        [self pauseMusicPlay];
        [self.navigationController pushViewController:[[CardViewController alloc] init] animated:YES];
    }else if(mode == MODE_UHOST){
        [self pauseMusicPlay];
        [self.navigationController pushViewController:[[CardViewController alloc] init] animated:YES];
    }else{
        if(appDele.globalManager && mode != MODE_A2DP){
            [appDele.globalManager setMode:MODE_A2DP];
        }
    }
}

-(void)pauseMusicPlay{
    //暂停本地音乐
    if(appDele.filePlayer && appDele.filePlayer.channelIsPlaying){
        [appDele playSong:MusicPlayStateModePause];
    }
}

-(void)dialogCancel{
    
}
-(void)batteryChanged:(UInt32)battery charging:(BOOL)charging{
    
}
-(void)lineinChanged:(BOOL)visibility{
    
}
-(void)dialogMessageArrived:(UInt32)type messageID:(UInt32)messageId{
    
}
-(void)hotplugCardChanged:(BOOL)visibility{
    //卡拔插
    appDele->hasCard = visibility;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hotplugUSBChanged" object:nil];
}
-(void)toastMessageArrived:(UInt32)messageId{
   
}
-(void)volumeChanged:(UInt32)current max:(UInt32)max min:(UInt32)min isMute:(BOOL)mute{
    appDele->curr_volue = current;
    appDele->maxVolue = max;
    appDele->minVoloe = min;
    appDele->isMute = mute;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"homeVolumeChanged" object:nil];
}

-(void)hotplugUSBChanged:(BOOL)visibility{
    
}
//U盘拔插
-(void)hotplugUhostChanged:(BOOL)visibility{
    appDele->hasUPan = visibility;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hotplugUhostChanged" object:nil];
}
-(void)daeModeChanged:(int)daeOpts {
    
}

-(void)soundEffectChanged:(UInt32)mode{
    
}
-(void)eqModeChanged:(UInt32)mode{
    
}
-(void)managerReady{

    self.selectedIndex=1;
    [self.navigationController.view hideToastActivity];
    //强制改为每次更新当前的连接地址
    if(appDele.autoConnManuter){
        NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
        [userDefaultes setObject:appDele.autoConnManuter forKey:@"manufacturerStr"];
        [userDefaultes synchronize];
    }
    
}
-(void)customCommandArrived:(UInt32)cmdKey param1:(UInt32)arg1 param2:(UInt32)arg2 others:(NSData *)data{
    int key = cmdKey & (~(0x4100));
    if(key == 131){
        
        int number= (int)(arg2 >> 16);
        appDele->currentA2dp=number;
        if (appDele->isfirstA2dp) {
        
            if (number==2) {
                
                UIAlertView *alter=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text",nil) message:NSLocalizedString(@"setAduio",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok_text",nil) otherButtonTitles:nil, nil];
                [alter show];
             appDele->isfirstA2dp=NO;   
                
            }
            
        }


        if(self.valueDelegate){
            
           [self.valueDelegate mainDateValueChangle:arg1 :arg2];
            
        }
    }
}

-(void)foundPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData{
    NSData *manufacturerData = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
    NSString *deviceName = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
    NSString *manufacturerStr = [manufacturerData description];  //NSData转NSString方法
    
    [self.navigationController.view hideToastActivity];  //隐藏
    for (NSMutableDictionary *dic in appDele.bluetoothDic) {  //过滤掉重复的蓝牙
        CBPeripheral *saved = [dic objectForKey:@"peripheral"];
        if (saved.identifier == peripheral.identifier) {
            return;
        }
    }
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    if(deviceName){
        [dict setObject:deviceName forKey:@"name"];
    }
    [dict setObject:peripheral forKey:@"peripheral"];
    if(manufacturerStr){
        [dict setObject:manufacturerStr forKey:@"manufacturerStr"];   //老版本可能没有 否则会异常
    }
    [appDele.bluetoothDic addObject:dict];
    if(appDele.autoConnManuter && [appDele.autoConnManuter isEqualToString:manufacturerStr]){
        //进行自动回连
        [appDele.mBluzConnector connect:peripheral];
        [appDele.mBluzConnector scanStop];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshBluetooth" object:nil];//通知刷新蓝牙
}
-(void)connectedPeripheral:(CBPeripheral *)peripheral{
    //蓝牙连接成功  弹出蓝牙成功页面
    [self.navigationController.view hideToastActivity];
    [appDele.mBluzConnector scanStop];//停止搜索蓝
    appDele.mMediaManager=[[BluzManager alloc] initWithConnector:appDele.mBluzConnector];
    appDele.globalManager=[appDele.mMediaManager getGlobalManager:self];
    appDele.connCBPeripheral = peripheral;
    appDele->isfirstA2dp=YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshBluetooth" object:nil];
//     [[NSNotificationCenter defaultCenter] postNotificationName:@"connectedPeripheral" object:nil];
}
-(void)disconnectedPeripheral:(CBPeripheral *)peripheral{
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        if(appDele.isOpenBlue){   //仅在蓝牙开启状态下断开才进行这个提示
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text",nil) message:NSLocalizedString(@"connection_close",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok_text",nil) otherButtonTitles:nil, nil];
//            alertView.tag = 100;
//            [alertView show];
//            [appDele.mBluzConnector scanStart];  //开始搜索蓝牙
//        }else{                    //现在蓝牙连接状态下突然关闭
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"hint_text",nil) message:NSLocalizedString(@"ihone_bluetooth_close",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok_text",nil) otherButtonTitles:nil, nil];
//            alertView.tag = 102;
//            [alertView show];
//        }
//    });
  
    //暂停本地音乐播放
    if(appDele.filePlayer && appDele.filePlayer.channelIsPlaying){
        
        [appDele playSong:MusicPlayStateModePause];
    }
    [self setSelectedIndex:0];   //回到第一页
    [appDele.mMediaManager close];
    [appDele.globalManager close];   //关键释放方法
    appDele.mMediaManager = nil;
    appDele.globalManager = nil;
    appDele.connCBPeripheral = nil;
    [appDele.mBluzConnector scanStart];  //开始搜索蓝牙
    //蓝牙重新进行搜索需要清空列表
    [appDele.bluetoothDic removeAllObjects];
     [TipHintView hideTipHint];
    //发送广播出去
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshBluetooth" object:nil];
    //针对非PUSH的页面
    [[NSNotificationCenter defaultCenter] postNotificationName:@"disconnectionBluetooth" object:nil];
//    //回到主页面
//    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
