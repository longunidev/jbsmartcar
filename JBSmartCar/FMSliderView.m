//
//  FMSliderView.m
//  CCA
//
//  Created by 肖建明 on 16/7/12.
//  Copyright © 2016年 Veryed. All rights reserved.
//

#import "FMSliderView.h"

@implementation FMSliderView{
    float padding;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initImage];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self initImage];
    }
    return self;
}
-(void)setFMValue:(int)value{
    //外部设置当前值
    float pointX =(value - 860)/240.0f*(self.frame.size.width-2*padding);
    [UIView beginAnimations:nil context:nil];
    self.pointImageView.center = CGPointMake(pointX+padding, self.pointImageView.center.y);
    [UIView commitAnimations];
}

-(void)initImage{
    //初始化控件
    self.sliderImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"slider_scale"]];
    self.pointImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"slider_handle"]];
}
-(void)layoutSubviews{
    if(self.subviews.count == 0){
        padding = self.frame.size.width *0.0625;
        self.sliderImageView.frame = CGRectMake(padding, self.frame.size.height*0.45, self.frame.size.width-padding*2, self.frame.size.height*0.55);
        [self addSubview:self.sliderImageView];
        //加入指针
        self.pointImageView.frame = CGRectMake(padding, 0, self.frame.size.width*0.08, self.frame.size.height*0.8);
        [self addSubview:self.pointImageView];
    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:[touch view]];
    [self touchListener:touchPoint];
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint touchPoint = [touch locationInView:[touch view]];
    [self touchListener:touchPoint];
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    //获取触摸点
    CGPoint pt = [touch locationInView:[touch view]];
    [self touchListener:pt];
}
-(void)touchListener:(CGPoint)touchPoint{
    if(touchPoint.x >= padding && touchPoint.x <= self.frame.size.width-padding){
        int volue = ((touchPoint.x-padding)/(self.frame.size.width-2*padding))*240+860;
        self.pointImageView.center = CGPointMake(touchPoint.x, self.pointImageView.center.y);
        if(self.volueDelegate){
            [self.volueDelegate fmVolueChangeDelegate:self :volue];
        }
    }else if(touchPoint.x > self.frame.size.width-padding){
        int volue = 1100;
        self.pointImageView.center = CGPointMake(self.frame.size.width-padding, self.pointImageView.center.y);
        if(self.volueDelegate){
            [self.volueDelegate fmVolueChangeDelegate:self :volue];
        }
    }else if(touchPoint.x < padding){
        int volue = 860;
        self.pointImageView.center = CGPointMake(padding, self.pointImageView.center.y);
        if(self.volueDelegate){
            [self.volueDelegate fmVolueChangeDelegate:self :volue];
        }
    }
}

@end
