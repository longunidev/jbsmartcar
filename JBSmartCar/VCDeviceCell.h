
#import <UIKit/UIKit.h>

@interface VCDeviceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *DeviceTitle;   //蓝牙名称
@property (weak, nonatomic) IBOutlet UILabel *DeviceState;    //蓝牙

@end
