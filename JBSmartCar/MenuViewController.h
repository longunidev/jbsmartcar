//
//  MenuViewController.h
//  JBSmart_Car
//
//  Created by yangxiao on 16/7/9.
//  Copyright © 2016年 yangxiao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XMSDK.h"
#import "UIImageView+WebCache.h"

@interface MenuViewController : UIViewController
{
    AppDelegate *appDete;
}
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UITableView *menuTableview;
@property (nonatomic,strong)NSMutableArray *datas;
@property (nonatomic,strong)NSNumber *categoteID;
@property (nonatomic,assign)NSString *categoteName;

@end
