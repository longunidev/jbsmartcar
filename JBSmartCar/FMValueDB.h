//
//  FMValueDB.h
//  ShiJie
//
//  Created by 肖建明 on 16/3/16.
//  Copyright © 2016年 Veryed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface FMValueDB : NSObject{
    sqlite3 *database;
}

-(BOOL)opneDatabase;    //打开数据库
-(void)closeDB;
-(NSMutableArray *)getAllFM;   //获取所有的音乐
-(BOOL)addFMValue:(int)fmValue;
-(BOOL)deleteFmValue:(int)fmValue;  //删除一个
@end
