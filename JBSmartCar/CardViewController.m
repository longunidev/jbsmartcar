//
//  CardViewController.m
//  JBSmartCar
//
//  Created by xjm on 17/3/1.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import "CardViewController.h"
#import "CardTableViewCell.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"
#import "CNPPopupController.h"
#import "UIView+Extension.h"
#import "MarqueeLabel.h"

@interface CardViewController ()<UITableViewDelegate,UITableViewDataSource,MusicDelegate>{
    AppDelegate* appDele;   //根对象，获取蓝牙连接对象
    NSMutableArray* m_arrCBLPLMusic;
    NSInteger m_nMusicState;   //当前播放状态
    NSInteger m_nSeq;   //当前播放下标
    int m_nCBLSongNum;   //歌曲总数
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet MarqueeLabel *musicName;

@property (nonatomic, strong) CNPPopupController *soundController;
@property (weak, nonatomic) IBOutlet UISlider *soundSliderView;
@property (weak, nonatomic) IBOutlet UILabel *soundHintLable;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@property (weak, nonatomic) IBOutlet UIButton *repeatButton;


@end

@implementation CardViewController

- (IBAction)repeatButtonClick:(id)sender {
    if(appDele->currCardMode == LOOP_MODE_ALL){ //列表循环
        [appDele.musicManager setLoopMode:LOOP_MODE_SINGLE];
    }else if(appDele->currCardMode == LOOP_MODE_SINGLE){   //单曲循环
        [appDele.musicManager setLoopMode:LOOP_MODE_SHUFFLE];
    }else if(appDele->currCardMode == LOOP_MODE_SHUFFLE){   //随机播放
        [appDele.musicManager setLoopMode:LOOP_MODE_ALL];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    appDele = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self.tableView registerNib:[UINib nibWithNibName:@"CardTableViewCell" bundle:nil] forCellReuseIdentifier:@"CardMusicTableCell"];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setRowHeight:60.0f];
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    m_arrCBLPLMusic = [[NSMutableArray alloc] init];
    
    if(appDele->curr_mode == MODE_CARD){
        [self.iconImage setImage:[UIImage imageNamed:@"tf_music_picture"]];
        [self.titleLabel setText:NSLocalizedString(@"tf_music",nil)];
    }else{
        [self.iconImage setImage:[UIImage imageNamed:@"u_music_picture"]];
        [self.titleLabel setText:NSLocalizedString(@"upan_music",nil)];
    }
    
    //获取代理
    if(!appDele.musicManager){
        appDele.musicManager = [appDele.mMediaManager getMusicManager:self];
    }else{
        appDele.musicManager = [appDele.mMediaManager getMusicManager:self];
        m_nCBLSongNum = [appDele.musicManager getPListSize];
        //[self getPlist];  //开始获取音乐列表
        if(m_nCBLSongNum > 0 ){
            [self.view makeToastActivity:CSToastPositionCenter];
            [appDele.musicManager getPListFrom:0 withCount:m_nCBLSongNum];
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeVolumeChanged) name:@"homeVolumeChanged" object:nil];
}

-(void)homeVolumeChanged{
    if(appDele.globalManager && self.soundController){
        [self.soundSliderView setValue:appDele->curr_volue];
        self.soundHintLable.text = [NSString stringWithFormat:@"%@:%i", NSLocalizedString(@"volume_text",nil) ,appDele->curr_volue];
    }
}

- (IBAction)soundButtoClick:(id)sender {
    if(!self.soundController){
        UIView *soundView = [[[NSBundle mainBundle] loadNibNamed:@"CardSoundPopView" owner:self options:nil] objectAtIndex:0];
        soundView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, soundView.height);
        self.soundSliderView.minimumTrackTintColor = [UIColor whiteColor];
        self.soundSliderView.maximumTrackTintColor = [UIColor grayColor];
        [self.soundSliderView setThumbImage:[UIImage imageNamed:@"music_slider_circle"]forState:UIControlStateNormal];
        
        self.soundController = [[CNPPopupController alloc] initWithContents:@[soundView]];
        self.soundController.theme = [CNPPopupTheme defaultTheme];
        self.soundController.theme.popupStyle = CNPPopupStyleActionSheet;
    }
    [self.soundSliderView setMinimumValue:appDele->minVoloe];
    [self.soundSliderView setMaximumValue:appDele->maxVolue];
    [self.soundSliderView setValue:appDele->curr_volue];
    self.soundHintLable.text = [NSString stringWithFormat:@"%@:%i", NSLocalizedString(@"volume_text",nil) ,appDele->curr_volue];
    [self.soundController presentPopupControllerAnimated:YES];
}

- (IBAction)soundSeekBarChange:(UISlider *)sender {
    if(appDele.globalManager){
        [appDele.globalManager setVolume:(int)sender.value];
        self.soundHintLable.text = [NSString stringWithFormat:@"%@:%i", NSLocalizedString(@"volume_text",nil) ,(int)sender.value];
    }
}

- (void)getPlist {     //获取下一个5条
    UInt32 left = (UInt32)(m_nCBLSongNum - m_arrCBLPLMusic.count);
    [appDele.musicManager getPListFrom:(UInt32)m_arrCBLPLMusic.count+1 withCount:(left >= 5 ? 5:left)];
    [self.view makeToastActivity:CSToastPositionCenter];
}
-(void)pListEntryReady:(NSMutableArray *)entryList{
    [m_arrCBLPLMusic addObjectsFromArray:entryList];   //加入到列表
    if(m_arrCBLPLMusic.count < m_nCBLSongNum){   //还没获取完毕
        [self.tableView reloadData]; //刷新列表
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:m_arrCBLPLMusic.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        [self getPlist];   //继续获取
    }else{
        [self.view hideToastActivity];
        [self.tableView reloadData]; //刷新列表
        [self.view makeToast:NSLocalizedString(@"song_load_end", nil) duration:2.0 position:CSToastPositionCenter];
        if(m_nSeq < [m_arrCBLPLMusic count]){
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:m_nSeq-1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];//设置选中列表
        }
    }
}
-(void)lyricReady:(UInt32)index lyric:(NSData *)lyric{
    
}
-(void)contentChanged{
    
}
-(void)stateChanged:(UInt32)state{
    appDele.m_nMusicState = state;
    if(state == STATE_PLAYING){
        [self.playButton setImage:[UIImage imageNamed:@"music_stop"] forState:UIControlStateNormal];
        self.musicName.labelize = NO;
    }else{
        [self.playButton setImage:[UIImage imageNamed:@"music_play"] forState:UIControlStateNormal];
        self.musicName.labelize = YES;
    }
    [self.view hideToastActivity];
}
-(void)musicEntryChanged:(MusicEntry *)entry{
    //播放对象改变
    m_nSeq = entry.index;   //赋值当前播放下标
    //设置当前的播放歌曲名称
    [self.musicName setText:entry.name];
    [self.musicName setLabelize:NO];
    if(entry.index < [m_arrCBLPLMusic count]){
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:entry.index-1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];//设置选中列表
    }
    [self.view hideToastActivity];
}

-(void)loopModeChanged:(UInt32)mode{
    appDele->currCardMode = mode;
    if(mode == LOOP_MODE_ALL){ //列表循环
        [self.repeatButton setImage:[UIImage imageNamed:@"loop_playback"] forState:UIControlStateNormal];
        [self showMiddleHint:NSLocalizedString(@"list_loop", nil)];
    }else if(mode == LOOP_MODE_SINGLE){   //单曲循环
        [self.repeatButton setImage:[UIImage imageNamed:@"single_tune_circulation"] forState:UIControlStateNormal];
        [self showMiddleHint:NSLocalizedString(@"onlne_play", nil)];
    }else if(mode == LOOP_MODE_SHUFFLE){   //随机播放
        [self.repeatButton setImage:[UIImage imageNamed:@"random_play"] forState:UIControlStateNormal];
        [self showMiddleHint:NSLocalizedString(@"radom_play", nil)];
    }
}

-(void)showMiddleHint:(NSString *)showContent{
    [self.view makeToast:showContent duration:2.0 position:CSToastPositionCenter];
}

-(void)managerReady:(UInt32)mode{
    [m_arrCBLPLMusic removeAllObjects];
    [self.tableView reloadData];   //刷新页面
    m_nCBLSongNum = [appDele.musicManager getPListSize];  //歌曲总数
    [self getPlist];   //开始获取音乐
    [self.view makeToastActivity:CSToastPositionCenter];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return m_arrCBLPLMusic.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CardMusicTableCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    MusicEntry *data = [m_arrCBLPLMusic objectAtIndex:indexPath.row];
    cell.indexLable.text =[NSString stringWithFormat:@"%ld", (long)indexPath.row+1];
    cell.musicName.text = data.name;
//    cell.layer.transform=CATransform3DMakeScale(0.1, 0.1, 1);
//    [UIView animateWithDuration:0.5 animations:^{
//        cell.layer.transform=CATransform3DMakeScale(1, 1, 1);
//    }];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [appDele.musicManager select:(indexPath.row+1)];
    [self.view makeToastActivity:CSToastPositionCenter];
}

- (IBAction)backButtonClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)playButtonClick:(id)sender {
    if(appDele.musicManager){
        if(appDele.m_nMusicState == STATE_PLAYING){
            [appDele.musicManager pause];
        }else{
            [appDele.musicManager play];
        }
        [self.view makeToastActivity:CSToastPositionCenter];
    }
    
}
- (IBAction)nextButtonClick:(id)sender {
    if(appDele.musicManager){
        [appDele.musicManager next];
        [self.view makeToastActivity:CSToastPositionCenter];
    }
}


@end
