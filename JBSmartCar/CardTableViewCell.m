//
//  CardTableViewCell.m
//  LTGM
//
//  Created by 肖建明 on 15/12/29.
//  Copyright © 2015年 wangwei. All rights reserved.
//

#import "CardTableViewCell.h"

@implementation CardTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected){
        self.tfPlayEQ.state = NAKPlaybackIndicatorViewStatePlaying;
        self.tfPlayEQ.tintColor=[UIColor orangeColor];
        self.tfPlayEQ.hidden=NO;
        self.indexLable.hidden=YES;
        self.musicName.textColor=[UIColor orangeColor];
        self.indexLable.textColor=[UIColor orangeColor];
        
        self.musicName.labelize = NO;
        
    }else{
        self.tfPlayEQ.state = NAKPlaybackIndicatorViewStateStopped;
        self.tfPlayEQ.hidden=YES;
        self.indexLable.hidden=NO;
        UIColor *color=[UIColor colorWithRed:170/255.0 green:170/255.0 blue:170/255.0 alpha:1.0];
        self.indexLable.textColor=color;
        
        self.musicName.labelize = YES;
        
        self.musicName.textColor=[UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0];
    }
}

@end
