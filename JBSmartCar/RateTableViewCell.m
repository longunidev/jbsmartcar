//
//  RateTableViewCell.m
//  JBSmartCar
//
//  Created by xjm on 17/3/16.
//  Copyright © 2017年 BTW. All rights reserved.
//

#import "RateTableViewCell.h"

@implementation RateTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected || self.isSelectFM){
        self.fmLabel.textColor = [UIColor orangeColor];
    }else{
        self.fmLabel.textColor = [UIColor colorWithWhite:1 alpha:0.8f];
    }
}

@end
